std::string toString(int integer)
{	
	if(!integer){
		return "0";
	}
	std::string res = "";
	const int maxSize = 10;
	int sizeCopy = maxSize - 1;
	char arr[maxSize];
	while (integer)
	{
		arr[sizeCopy--] = integer % 10;
		integer /= 10;
		if (sizeCopy == -1){
			break;
		}
	}
	sizeCopy++;
	for (int i = sizeCopy; i < maxSize; i++)
	{
		res += arr[i] + 48;
	}
	return res;
}
std::string* strToWordsArr(std::string string, int maxWordArrSize, int &resLength)
{
	int	         stringLength = string.length();
	std::string* wordArr      = new std::string[maxWordArrSize];
	int			 lastWordPos  = 0;
	resLength = 0;
	
	for(int charPos = 0; charPos < stringLength; charPos++)
	{
		if (string[charPos] == ' ' && resLength < maxWordArrSize){
			wordArr[resLength++] = string.substr(lastWordPos, charPos - lastWordPos);
			lastWordPos = charPos + 1;
		}
	}
	if (resLength < maxWordArrSize){
		wordArr[resLength++] = string.substr(lastWordPos, stringLength - lastWordPos);
	}
	return wordArr;
}
std::string reverse(std::string str)
{
	int         length = str.length();
	std::string result = "";
	for(int i = length - 1; i >= 0; i--)
	{
		result += str[i];
	}
	return result;
}
class Chronicle
{
	std::string **messages;
	int length,
		position;
		
	std::string head,
				empty,
				foot;
public:
	void setMessage(std::string mess)
	{
		int messLength      = mess.length(),
			newArrResLength = 0,
			newArrLength    = 10,
			wordStartPos    = 0,
			temp            = 0;
		std::string *messArr  = new std::string[newArrLength];
		std::string emptyLine = "";
		for(int i = 0; i < messLength; i++)// \n clear
		{
			if(mess[i] == '\n'){
				mess[i] = ' ';
			}
		}
		for(int i = 0; i < newArrLength; i++)//clear
		{
			messArr[i] = "";
		}
		while(true)
		{
			if(newArrResLength == newArrLength){
				break;
			}
			
			if(messLength <= 78){
				
				while(mess.length() < 78)
				{
					mess += ' ';
				}
				messArr[newArrResLength++] = mess;
				break;
			}
			wordStartPos = 77;
			while(mess[wordStartPos] != ' '){ --wordStartPos;}
			messArr[newArrResLength] = mess.substr(0, wordStartPos + 1);
			temp = 78 - wordStartPos - 1;
			for(int i = 0; i < temp; i++)
			{
				messArr[newArrResLength] += ' ';
			}
			newArrResLength++;
			messLength -= wordStartPos;
			mess = mess.substr(wordStartPos + 1, messLength);
		}
		if(newArrResLength + position - 1>= length){
			std::string **newMessagesArr = new std::string*[length];
			for(int i = 0; i < length; i++)
			{
				newMessagesArr[i] = new std::string("");
			}
			position = 0;
			for(int i = 0; i < length; i++)
			{
				if(i > length - 5){
					if(*messages[i] != ""){
						*newMessagesArr[position++] = *messages[i];
					}
				} 
				delete messages[i];
			}
			delete [] messages;
			messages = newMessagesArr;
		}
		for(int i = 0; i < newArrResLength; i++)
		{
			if(position < length){
				*messages[position++] = messArr[i];
			} else {
				break;
			}
		}
		delete [] messArr;
	}
	void print()
	{
		std::cout << head;
		for(int i = 0; i < length; i++)
		{
			if( i < position ){
				std::cout << '\xB3' << *messages[i] << '\xB3';
			} else {
				std::cout << empty;
			}
		}
		std::cout << foot;
	}
	Chronicle(int maxHeight):length(maxHeight), position(0), head(""), foot(""), empty("")
	{
		char lu = '\xDA', ru = '\xBF', g = '\xC4', v = '\xB3',  ld = '\xC0', rd = '\xD9';
		messages = new std::string*[length];
		for(int i = 0; i < length; i++)
		{
			messages[i] = new std::string("");
		}
		empty= v;
		foot = ld;
		for(int i = 0; i < 78; i++)
		{
			head += g;
			empty+= ' ';
		}
		foot += head;
		foot += rd;
		head = lu + head + ru;
		empty+= v;
		
		#ifdef DEBUG
			std::cout << "Chronicle was created.\n";
		#endif
	}
	~Chronicle()
	{
		for(int i = 0; i < length; i++)
		{
			delete messages[i];
		}
		delete [] messages;
		#ifdef DEBUG
			std::cout << "Chronicle was deleted.\n";
		#endif
	}
};

std::string* multipleMessCheck(std::string mess, int &length)
{
	int messQ      = 0,
		messLength = mess.length();
		
	std::string *result = NULL;
		
	for(int i = 0; i < messLength; i++)
	{
		if(mess[i] == '\n'){
			messQ++;
		}
	}
	if(mess[messLength - 1] == '\n'){
		if(messQ == 1){
			mess =  mess.substr(0, messLength - 1);
			messQ = 0;
		}
	} else if(messQ){
		if(messQ == 1){
			messQ = 0;
		} else {
			mess += '\n';
			messQ++;
		}
	}
	if(messQ){
		result = new std::string[messQ];
		int messEndPos = 0,
			resultPos  = 0;
		for(int i = 0; i < messLength; i++)
		{
			if(mess[i] == '\n'){
				result[resultPos++] = mess.substr(messEndPos, i - messEndPos);
				messEndPos = i + 1;
			}
		}
		length = messQ;
	} else {
		length    = 1;
		result    = new std::string[length];
		result[0] = mess;
	}
	return result;
	
}

