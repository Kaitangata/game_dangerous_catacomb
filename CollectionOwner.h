class Display
{
public:
	virtual void display() = 0;
	
	Display()
	{
		#ifdef DEBUG
			std::cout << "Display - ";
		#endif
	}
	virtual ~Display()
	{
		#ifdef DEBUG
			std::cout << "Display was deleted.\n";
		#endif
	}
};

class Observer: public Display
{
public:
	virtual void update(int)= 0;
	
	Observer(): Display()
	{
		#ifdef DEBUG
			std::cout << "Observer - ";
		#endif
	}
	~Observer()
	{
		#ifdef DEBUG
			std::cout << "Observer -";
		#endif
	}
};

template <class Type>
class IterObserver: public Display
{
public:
	virtual void update(Iterator<Type> &object)= 0;
	
	IterObserver(): Display()
	{
		#ifdef DEBUG
			std::cout << "IterObserver - \n";
		#endif
	}
	~IterObserver()
	{
		#ifdef DEBUG
			std::cout << "- IterObserver - ";
		#endif
	}
};

template <class Type>
class MenuItemDisplay: public IterObserver<Type>
{
private:
	Iterator<Type> _iterator;
public:
	void update(Iterator<Type> &iter)
	{		
		_iterator = iter;
		display();
	}
	
	void display()
	{
		int pos = _iterator.current();
		_iterator.first();
		std::string str = "";
		while(_iterator.hasNext()){
			if (pos == _iterator.current()){
				str = char(16);
			} else {
				str = ' ';
			}
			str += _iterator.nextTitle();
			printCenterWord(80, str);
			//_iterator.nextPrintTitle();
			std::cout << '\n';
		}
		#ifdef DEBUG_EXT
			std::cout << "--End of array--\n";
		#endif
	}
	
	MenuItemDisplay(): IterObserver<Type>()
	{
		#ifdef DEBUG
			std::cout << "- MenuItemDisplay was created.\n";
		#endif
	}
	
	~MenuItemDisplay()
	{	
		#ifdef DEBUG
			std::cout << "MenuItemDisplay -\n";
		#endif
	}		
};

template <class Type>
class InventoryDisplay: public IterObserver<Type>
{
private:
	Type _iterator;
public:
		void update(Type iter)
		{	
			_iterator = iter;
			display();
		}
		
		void display()
		{////////////////////////////////////to change!
			int pos = _iterator.current();
			_iterator.first();
			while(_iterator.hasNext()){
				if (pos == _iterator.current()){
					std::cout << char(16);
				} else {
					std::cout << " ";
				}
				_iterator.nextPrintTitle();
			}
			#ifdef DEBUG_EXT
				std::cout << "--End of array--\n";
			#endif
		}
		
		InventoryDisplay(): IterObserver<Type>()
		{
			#ifdef DEBUG
				std::cout << "InventoryDisplay was created.\n";
			#endif
		}
		
		~InventoryDisplay()
		{	
			#ifdef DEBUG
				std::cout << "InventoryDisplay - ";
			#endif
		}		
};

class Notifier
{
public:
	virtual void notifyObserver	() = 0;
	
	Notifier()
	{
		#ifdef DEBUG
			std::cout << "(Notifier - ";
		#endif
	}
	virtual ~Notifier()
	{
		#ifdef DEBUG
			std::cout << "Notifier) was deleted.\n";
		#endif
	}
};

class Observable: public Notifier
{
public:
	virtual void setObserver (int) = 0;
	
	Observable(): Notifier()
	{
		#ifdef DEBUG
			std::cout << "Observable - ";
		#endif
	}
	virtual ~Observable()
	{
		#ifdef DEBUG
			std::cout << "Observable) was deleted.\n";
		#endif
	}
};

template <class Type>
class IterObservable: public Notifier
{
public:
	virtual void setObserver (IterObserver<Type>* observer) = 0;
	
	IterObservable(): Notifier()
	{
		#ifdef DEBUG
			std::cout << "IterObservable, ";
		#endif
	}
	~IterObservable()
	{
		#ifdef DEBUG
			std::cout << "IterObservable - ";
		#endif
	}
};

template <class Type>
class CollectionOwner: public IterObservable<Type>, public Aggregate<Type>
{
public:
	virtual void 		push(Type)		= 0;
	virtual Type 		pop()			= 0;
	//virtual void 		choiseNextItem()= 0;
	//virtual void 		choisePrevItem()= 0;
	
	CollectionOwner(): IterObservable<Type>(), Aggregate<Type>()
	{
		#ifdef DEBUG
			std::cout << "CollectionOwner - ";
		#endif
	}
	~CollectionOwner()
	{
		#ifdef DEBUG
			std::cout << "CollectionOwner (";
		#endif
	}
};

template <class Type>
class Owner: public CollectionOwner<Type>
{
	Container<Type>*	_container;
	IterObserver<Type>*	_observer;
	int					chosedItemPos;
	void correctPos()
	{
		int length = _container->length();
		if(chosedItemPos == length){
			chosedItemPos = 0;
		}
		
		if(chosedItemPos < 0){
			chosedItemPos = length - 1;
		}
	}
public:
	Iterator<Type> createIterator(bool withCurrentPos)
	{
		correctPos();
		Iterator<Type> it = _container->iterator();
		if(withCurrentPos){
			it.current(chosedItemPos);
		}
		return it;
	}
	
	void push(Type object)
	{
		_container->push(object);
		notifyObserver();
	}

	Type pop()
	{
		Type resObject = _container->pop();
		notifyObserver();
		return resObject;
	}
	
	std::string getCurrentElementTitle()
	{
		Iterator<Type> it = createIterator(true);
		Type object = it.next();
		return object.title();
	}

	void setObserver(IterObserver<Type>* objDisp)
	{
		_observer = objDisp;
	}
	
	void notifyObserver()
	{
		if(_observer){
			Iterator<Type> it = createIterator(true);
			_observer->update(it);
		}
	}

	void choiseNextItem()
	{
		++chosedItemPos;
		notifyObserver();
	}

	void choisePrevItem()
	{
		--chosedItemPos;
		notifyObserver();
	}

	Owner(Container<Type>* container, IterObserver<Type>* observer = NULL
	): CollectionOwner<Type>(), _container(container), _observer(observer), chosedItemPos(0)
	{
		#ifdef DEBUG
			std::cout << "Owner was created.\n";
		#endif
	}
	
	~Owner()
	{
		delete _observer;
		delete _container;
		
		#ifdef DEBUG
			std::cout << "Owner - ";
		#endif
	}
};
