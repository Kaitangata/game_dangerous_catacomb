//#define DEBUG_EXT
#ifdef DEBUG_EXT
	#define DEBUG
#endif
//#define Friendly Hero
#define STARTSTATE "Menu"
#include <conio.h>
#include <time.h>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include "Sandbox.h"
#include "Printer.h"
#include "GameLib.h"
int main()
{
	srand(time(NULL));
	
	Controller       *control  = new Controller(new Up(), new Right(), new Accept());
	StateFactory     *sFactory = new StateFactory();
	CharacterFactory *cFactory = new CharacterFactory();
	GameItemFactory  *gFactory = new GameItemFactory();
	Chronicle        *chronicle= new Chronicle(20);
	
	GameCore gc = GameCore(control, sFactory, cFactory, gFactory, chronicle);
	gc.loop();
	return 0;          
}
