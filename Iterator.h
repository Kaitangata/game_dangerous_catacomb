template <class type>
class Iterator
{
private:
	type*	array;
	int 	_length;
	int 	innerPos;
public:
	bool hasNext()
	{
		if(array && (innerPos < _length)){
			return true;
		} else {
			return false;
		}
	}
	bool hasPrintNext(int pos = 0)
	{
		if (array && (innerPos < _length)){
			if (array[innerPos].getLineOfPicture(pos) == ""){
			} else {
				return true;
			}
		}
		return false;
		
	}
	type next()
	{
		return array[innerPos++];
	}
	std::string nextTitle()
	{
		return array[innerPos++].title();
	}
	void nextPrintLine(int pos = 0)
	{
		array[innerPos++].printLineOfPicture(pos);
	}
	int length()
	{
		return _length;
	}				
	void first()
	{
		innerPos = 0;
	}
	void end()
	{
		innerPos = _length - 1;
	}		
	int current()
	{
		return innerPos;
	}			
	void current(int pos)
	{
		innerPos = pos;
		if(innerPos > _length){
			end();
		}

		if(innerPos < 0){
			first();
		}
	}				
	Iterator(type*	arr = NULL, int length = 0)
	: array(arr), _length(length), innerPos(0)
	{
		#ifdef DEBUG
			std::cout << "Iterator was created.\n";
		#endif
	}

	~Iterator()
	{
		#ifdef DEBUG
			std::cout << "Iterator was deleted.\n";
		#endif
	}

};

template <class type>
class Aggregate
{
public:
	virtual Iterator<type> createIterator(bool withCurrentPos = false) = 0;

	Aggregate()
	{
		#ifdef DEBUG
			std::cout << " Aggregate) -";
		#endif
	}
	
	virtual ~Aggregate()
	{
		#ifdef DEBUG
			std::cout << "Aggregate, ";
		#endif
	}
};
