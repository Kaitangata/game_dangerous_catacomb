template <class Type>
class Container
{
	int 	_length;
	Type* 	_array;
public:
	int length()
	{
		return _length;
	}
	void push(Type object)
	{
		Type* newArray = new Type[_length + 1];
		for (int i = 0; i < _length; i++)
		{
			newArray[i] = _array[i];
		}
		newArray[_length] = object;
		delete [] _array;	
		_array = newArray;
		_length++;
	}
	Type pop()
	{	
		Type resObject = Type("Empty");
		if (_length > 1){
			--_length;
			Type* newArray = new Type[_length];
			for (int i = 0; i < _length; i++)
			{
				newArray[i] = _array[i];
			}
			resObject = _array[_length];
			delete [] _array;
			_array = newArray;
		} else if (_length){
			resObject = _array[0];
			delete [] _array;
			_array = NULL;
			_length = 0;
		}
		return resObject;
	}
	Iterator<Type>	iterator()
	{
		return Iterator<Type>(_array, _length);
	}
	void operator=(Container<Type> &copyed)
	{
		_length = copyed._length;
		_array = copyed._array;
	}
	Container(Container<Type> &copyed)
	{
		_length = copyed._length;
		_array = copyed._array;
	}
	Container(Type* array, int length): _array(array), _length(length)
	{
		#ifdef DEBUG
			std::cout << "Container was created.\n";
		#endif
	}
	~Container()
	{
		delete [] _array;
		#ifdef DEBUG
			std::cout << "Container was deleted.\n";
		#endif
	}
};
