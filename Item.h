class Character;
class Item
{
protected:
	std::string _title;
	std::string _discription;
	std::string _type;
public:
	virtual bool         printLineOfPicture(int pos) = 0;
	virtual std::string  getLineOfPicture  (int pos) = 0;
	virtual std::string* getPicture        ()        = 0;
	virtual int          getPictureLength  ()        = 0;
	std::string title(std::string newTitle= "")
	{
		if(newTitle != ""){
			_title = newTitle;
		}
		return _title;
	}
	std::string discription(std::string newDiscript = "")
	{
		if(newDiscript != ""){
			_discription = newDiscript;
		}
		return _discription;
	}
	std::string type()
	{
		return _type;
	}
	void printTitle()
	{
		std::cout << _title;
	}
	Item(
	    std::string title, 
		std::string discription, 
		std::string type
	): _title(title), _discription(discription), _type(type)
	{
		#ifdef DEBUG
			std::cout << "Item (" << _title << ") - ";
		#endif
	}
	virtual ~Item()
	{
		#ifdef DEBUG
			std::cout << "Item(" << _title << ") was deleted\n";
		#endif
	}
};
class ExtItem: public Item
{
protected:
    int  _expiration, 
         _value,
         _pictureLength,
         _chance;
    bool _destructible,
         _repeatable,
         _pass;
    std::string  _subtype;
	std::string* _picture;
public:
	virtual std::string className() = 0;
	virtual std::string capture(Character*) = 0;
	virtual std::string release(Character*) = 0;
	virtual std::string update (Character*) = 0;
	
	virtual void info()
	{
		std::cout << "-----------ITEM_INFO-----------\nTitle: "<< _title <<"\n-------------------------------\n";
	}
	int expiration()
	{
		return _expiration;
	}
    int value()
    {
    	return _value; 
	}
	int chance(bool _return = false)
	{
		if(!_chance || _return){
			return _chance;
		}
		int result = 0,
		    fraction = 100 / _chance,
		    residue  = 100 % _chance;
		if(residue){
			result = rand() % static_cast<int>((100.0 / _chance) * 100);
			if( result > 100 ){
				result = 0;
			}
		} else {
			result = !( rand() % fraction );
		}
		return result;	
	}
	std::string subtype(std::string sub = "")
    {
    	if(sub != ""){
    		_subtype = sub;
		}
    	return _subtype; 
	}
    bool printLineOfPicture(int pos)
	{
		if( _picture && (pos < _pictureLength) && (pos >= 0) ){
			printCombination(_picture[pos]);
			if(pos < _pictureLength - 1){
				return true;
			} else {
				return false;
			}
		} 
		return false;
	}
	std::string getLineOfPicture(int pos)
	{
		if( _picture && (pos < _pictureLength) && (pos >= 0) ){
			return _picture[pos];
		} 
		return "";
	}
	std::string* getPicture()
	{
		return _picture;
	}
	int getPictureLength()
	{
		return _pictureLength;
	}
    ExtItem(
    	std::string  title,
    	std::string  discription,
    	std::string  type,
    	std::string  subtype,
	    int/*     */ expiration,
		int/*     */ value,
		int/*     */ chance,
		bool/*    */ destructible,
		bool/*    */ repeatable,
		std::string* picture,
		int/*     */ pLength
	):  Item         (title, discription, type),
		_expiration  (expiration), 
	    _value       (value),
	    _pictureLength(pLength),
	    _picture     (picture),
	    _subtype     (subtype),
	    _destructible(destructible),
	    _repeatable  (repeatable),
		_pass        (true),
		_chance      (chance)

    {
    	#ifdef DEBUG
    		std::cout << "ExtItem - ";
    	#endif
	}
	~ExtItem()
	{
		#ifdef DEBUG
    		std::cout << "ExtItem - ";
    	#endif
	}
};
class StatChange: public ExtItem
{
	std::string _targetStat;
public:
	std::string capture(Character*);
	std::string release(Character*);
	std::string update (Character*);
	std::string className()
	{
		return "StatChange";
	}
	void info()
	{
		std::cout << "-----------ITEM_INFO-----------\nClass: "<< className() <<"\nTitle: "
		<< _title << "\nType: " << _type << "\nSubtype: " << _subtype << "\nDestructible: "
		<< _destructible << "\nExpiration: " << _expiration << "\nRepeatable: " << _repeatable
		<< "\nPass: " << _pass << "\n-------------------------------\n";
	}
	StatChange(
		std::string  title/*    */= "Empty",
    	std::string  discription  = "",
    	std::string  type/*    */ = "",
    	std::string  subtype/*  */= "",
	    int/*     */ expiration   = 100,
		int/*     */ value/*    */= 0,
		int/*     */ chance/*   */= 10,
		bool/*    */ destructible = true,
		bool/*    */ repeatable   = false,
		std::string  targetStat   = "HP",
		std::string* picture/*  */= NULL,
		int/*     */ pLength/*  */= 0
	): ExtItem(title, discription, type, subtype, expiration, value, chance, destructible, repeatable, picture, pLength),
	   _targetStat(targetStat)
	{
		if(!repeatable){
			_pass = true;
		}
		#ifdef DEBUG
    		std::cout << "StatChange was created.\n";
    	#endif
	}
	~StatChange()
	{
		#ifdef DEBUG
			std::cout << "StatChange - ";
		#endif
	}
};
class TargetSlotChange: public ExtItem
{
public:
	std::string capture(Character*);
	std::string release(Character*);
	std::string update (Character*);
	std::string className()
	{
		return "TargetSlotChange";
	}
	void info()
	{
		std::cout << "-----------ITEM_INFO-----------\nClass: "<< className() <<"\nTitle: "
		<< _title << "\nType: " << _type << "\nSubtype: " << _subtype << "\nDestructible: "
		<< _destructible << "\nExpiration: " << _expiration << "\nRepeatable: " << _repeatable
		<< "\nPass: " << _pass << "\n-------------------------------\n";
	}
	TargetSlotChange(
		std::string  title/*    */= "Empty",
    	std::string  discription  = "",
    	std::string  type/*    */ = "",
    	std::string  subtype/*  */= "",
	    int/*     */ expiration   = 100,
		int/*     */ value/*    */= 0,
		int/*     */ chance/*   */= 10,
		bool/*    */ destructible = true,
		bool/*    */ repeatable   = false,
		std::string* picture/*  */= NULL,
		int/*     */ pLength/*  */= 0
	): ExtItem(title, discription, type, subtype, expiration, value, chance, destructible, repeatable, picture, pLength)
	{
		if(!repeatable){
			_pass = true;
		}
		#ifdef DEBUG
    		std::cout << "TargetSlotChange was created.\n";
    	#endif
	}
	~TargetSlotChange()
	{
		#ifdef DEBUG
			std::cout << "TargetSlotChange - ";
		#endif
	}
};
class EffectHolder: public ExtItem
{
public:
	EffectHolder(
		std::string  title,
    	std::string  discription,
    	std::string  type,
    	std::string  subtype,
	    int/*     */ expiration,
		int/*     */ value,
		int/*     */ chance,
		bool/*    */ destructible,
		bool/*    */ repeatable,
		std::string* picture,
		int/*     */ pLength
	):  ExtItem(title, discription, type, subtype, expiration, value, chance, destructible, repeatable, picture, pLength)
	{
		#ifdef DEBUG
			std::cout << "EffectHolder - ";
		#endif
	}
	~EffectHolder()
	{
		#ifdef DEBUG
			std::cout << "EffectHolder - ";
		#endif
	}
};
class GameItem: public EffectHolder
{
	ExtItem**  _effects;
	int        _eLength;
public:
	std::string capture(Character*){}
	std::string update (Character*);
	std::string className()
	{
		return "GameItem";
	}
	void info()
	{
		std::cout << "-----------ITEM_INFO-----------\nClass: "<< className() <<"\nTitle: "
		<< _title << "\nType: " << _type << "\nSubtype: " << _subtype << "\nDestructible: "
		<< _destructible << "\nExpiration: " << _expiration << "\nEffects:";
		for(int i = 0; i < _eLength; i++)
		{
			std::cout <<"\n    "<< _effects[i]->title() <<"(" <<  _effects[i]->subtype() << ")";
		}
		std::cout << "\n-------------------------------\n";
	}
	void info_buff()
	{
		for(int i = 0; i < _eLength; i++)
		{
			std::cout <<"\n        "<< _effects[i]->title() 
			<<"(" <<  _effects[i]->subtype() << ", " << _effects[i]->expiration()<< ")";
		}
	}
	void effectsExpRecalculate()
	{
		if(!_effects || !_eLength){
			return;
		}
		
		if(!_expiration){
			int effExpiration = 0;
			for (int i = 0; i < _eLength; i++)
			{
				effExpiration = _effects[i]->expiration();
				if(_expiration < effExpiration)
				_expiration = effExpiration;
			}
		}
	}
	ExtItem** getEffects(int &lengths)
	{
		lengths	= _eLength;
		return _effects;
	}
	void setEffects(ExtItem** array, int length)
	{
		if(!array || !length){
			#ifdef DEBUG
    			std::cout << "GameItem " << _title << ": gets empty effects array\n";
    		#endif
			return;
		}
		for (int i = 0; i < _eLength; i++)
		{
			delete _effects[i];
		}
		delete [] _effects;
		
		_effects = array;
		_eLength = length;
		effectsExpRecalculate();
	}
	void destroy()
	{
		_expiration -= rand() % 3;
		if(_expiration < 0){
			_expiration = 0;
		}
	}
	GameItem* debuff()
	{
		GameItem* item = NULL;
		if(_effects){
			int         arrIndex[_eLength] = {0};
			int         arrIndexLength     =  0;
		    ExtItem**   newEffArr          = NULL;
		    std::string className          = "";//temp
			for(int i = 0; i < _eLength; i++)
			{
				if(_effects[i]->subtype() == "Negative" && _effects[i]->chance()){
					arrIndex[arrIndexLength++] = i;
				}
			}
			if(arrIndexLength){
				newEffArr       = new ExtItem*[arrIndexLength];
				ExtItem* curEff = NULL;
				for(int i = 0; i < arrIndexLength; i++)
				{
					curEff = _effects[arrIndex[i]];
					className = curEff->className();
					if       ( className == "StatChange" ){
						newEffArr[i] = new StatChange();
					} else if( className == "TargetSlotChange" ){
					    newEffArr[i] = new TargetSlotChange();
					} else if( className == "SomeClass..." ){
					    newEffArr[i] = new TargetSlotChange();
					} else {
						#ifdef DEBUG
    						std::cout << "ERROR: Class not exists/ GameItem::debuff()\n";
    					#endif
						for(int i = 0; i < arrIndexLength; i++)
						{
							delete newEffArr[i];
							newEffArr[i] = NULL;
						}
						delete [] newEffArr;
						newEffArr = NULL;
						break;
					}
					*(newEffArr[i]) = *(curEff);
					newEffArr[i]->subtype("Positive");
				}
				if(newEffArr){
					item   = new GameItem(
						"The effect of the " + _title,
						"A gift from the admirer.",
						"Effect",
						"Negative",
						0,
						0,
						0,
						true,
						newEffArr,
						arrIndexLength,
						_picture,
						_pictureLength
					);
				}	
			} 
		}
		return item;
	}
	std::string release(Character* character)
	{
		std::string message = "";
		for(int i = 0; i < _eLength; i++)
		{
			_effects[i]->release(character);
			message += _effects[i]->title();
			if(i != _eLength - 1){
				message += ", ";
			}
		}
		return message;
	}
	void operator=(GameItem &gi)
	{
		std::string className          = "";
		int         copyedEffArrlength = gi._eLength;
		ExtItem**   newEffArr          = NULL;
		
		_title        = gi._title;
		_discription  = gi._discription;
		_type         = gi._type;
		_subtype      = gi._subtype;
		_destructible = gi._destructible;
		_expiration   = gi._expiration;
		_value        = gi._value;
		
		if(_effects){
			for (int i = 0; i < _eLength; i++)
			{
				delete _effects[i];
				_effects[i] = NULL;
			}
			delete [] _effects;
		}
		if(gi._effects){
			newEffArr = new ExtItem*[gi._eLength];
			for(int i = 0; i < gi._eLength; i++)
			{
				className = (gi._effects[i])->className();
				
				if       ( className == "StatChange" ){
					newEffArr[i] = new StatChange();
				} else if( className == "TargetSlotChange" ){
					newEffArr[i] = new TargetSlotChange();
				} else if( className == "SomeClass..." ){
				  //newEffArr[i] = new someclass();
				} else {
					#ifdef DEBUG
    					std::cout << "ERROR: Class not exists/ GameItem::operator(Character*)\n";
    				#endif
					for(int i = 0; i < gi._eLength; i++)
					{
						delete newEffArr[i];
						newEffArr[i] = NULL;
					}
					delete [] newEffArr;
					newEffArr = NULL;
					break;
				}
				*(newEffArr[i]) = *(gi._effects[i]);	
			}
		}
		_effects      = newEffArr;
	    _eLength      = gi._eLength;
        _pictureLength= gi._pictureLength;
	    _picture      = gi._picture;
	}
	GameItem(
		std::string  title/*  */ = "Empty",
    	std::string  discription = "",
    	std::string  type/*   */ = "",
    	std::string  subtype/**/ = "",
	    int/*     */ expiration  = 100,
		int/*     */ value/*  */ = 0,
		int/*     */ chance/* */ = 0,
		bool/*    */ destructible= true,
		ExtItem**    effects/**/ = NULL,
		int/*     */ eLength/**/ = 0,
		std::string* picture/**/ = NULL,
		int/*     */ pLength/**/ = 0
	):  EffectHolder(title, discription, type, subtype, expiration, value, chance, destructible, false, picture, pLength),
		_effects(effects), _eLength(eLength)
	{
		effectsExpRecalculate();
		#ifdef DEBUG
    		std::cout << "GameItem was created.\n";
    	#endif
	}
	~GameItem()
	{
		for (int i = 0; i < _eLength; i++)
		{
			delete _effects[i];
		}
		delete [] _effects;
		#ifdef DEBUG
			std::cout << "GameItem - ";
		#endif
	}	
};
class MenuItem: public Item
{
public:
	bool printLineOfPicture(int pos)
	{
		std::cout << _title;
		return true;
	}
	std::string getLineOfPicture(int pos)
	{
		return "";
	}
	std::string* getPicture()
	{
		return NULL;
	}
	int getPictureLength()
	{
		return 0;
	}
	MenuItem(
		std::string  title       = "Empty",
		std::string  discription = "",
		std::string  type        = ""
	): Item(title, discription, type)
	{
		#ifdef DEBUG
			std::cout << "MenuItem  was created\n";
		#endif
	}
	~MenuItem()
	{
		#ifdef DEBUG
			std::cout << "MenuItem - ";
		#endif
	}
};
