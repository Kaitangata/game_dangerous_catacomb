class GameCore;
class State
{
protected:
	GameCore* 	gameCore;
	State*		prevState;
public:
	virtual void up() 			=0;
	virtual void down() 		=0;
	virtual void left() 		=0;
	virtual void right() 		=0;
	virtual void accept()		=0;
	virtual void close()		=0;
	virtual void additional(std::string param = ""){}
	virtual void refresh(){}
	
	virtual std::string className()	=0;
	#ifdef DEBUG
		void info()
		{
			std::cout << className() << '-';
			if(prevState){
				prevState->info();
			} else {
				std::cout << "Empty";
			}
		}
	#endif
	State* 	search(std::string stateName, State** requester)
	{
		State* result = NULL;
		if(className() == stateName){
			if(*requester == this){
				*requester = prevState;
			} else {
				(*requester)->setPrevState(prevState);
			}
			prevState = NULL;
			result    = this;
		} else if(prevState){
			State **newRequester = new State*;
				   *newRequester = this;
			result = prevState->search(stateName, newRequester);
			delete newRequester;
		} 
		return result;
	}
	void setPrevState(State* prevs)
	{
		prevState = prevs;
	}

	State* getPrevState()
	{
		return prevState;
	}

	State(GameCore* gc): gameCore(gc), prevState(NULL)
	{
		#ifdef DEBUG
			std::cout << "State - ";
		#endif
	}
	
	virtual ~State()
	{
		delete prevState;
		#ifdef DEBUG
			std::cout << "State was deleted.\n";
		#endif
	}
};
///////COMMAND////////////////////////////////////////////////
class Command
{
public:
	virtual void execute(State* state) = 0;
	virtual void undo	(State* state) = 0;
	Command()
	{	
		#ifdef DEBUG
			std::cout << "Command(";
		#endif
	}
	virtual ~Command()
	{	
		#ifdef DEBUG
			std::cout << "was deleted.\n";
		#endif
	}
};
///////UP////////////////////////////////////////////////
class Up: public Command
{
public:
	void execute(State* state)
	{
		state->up();
	}
	void undo(State* state)
	{
		state->down();
	}
	Up():Command()
	{
		#ifdef DEBUG
			std::cout << "Up) was created.\n";
		#endif
	}
	~Up()
	{
		#ifdef DEBUG
			std::cout << "Command(Up) ";
		#endif
	}
};
///////RIGHT////////////////////////////////////////////////
class Right: public Command
{
public:
	void execute(State* state)
	{
		state->right();
	}
	void undo(State* state)
	{
		state->left();
	}
	Right():Command()
	{
		#ifdef DEBUG
			std::cout << "Right) was created.\n";
		#endif
	}
	~Right()
	{
		#ifdef DEBUG
			std::cout << "Command(Right) ";
		#endif
	}
};
///////ACCEPT////////////////////////////////////////////////
class Accept: public Command
{
public:
	void execute(State* state)
	{
		state->accept();
	}
	void undo(State* state)
	{
		state->close();
	}
	Accept():Command()
	{
		#ifdef DEBUG
			std::cout << "Accept) was created.\n";
		#endif
	}
	~Accept()
	{
		#ifdef DEBUG
			std::cout << "Command(Accept) ";
		#endif
	}
};
///////CONTROLLER////////////////////////////////////////////////
class Controller
{
	Command* _up;
	Command* _right;
	Command* _accept;
	Command* _additional;
	int keyFilter()
	{
		int getchResult = getch();
		if (getchResult == 224){
			return getch();
		}
		return getchResult;
	}
public:
	bool processor(State* state)
	{
		int buttonCode = keyFilter();

		if (buttonCode == 72){
			_up->execute(state);
		}
		if (buttonCode == 77){
			_right->execute(state);
		}
		if (buttonCode == 80){
			_up->undo(state);
		}
		if (buttonCode == 75){
			_right->undo(state);
		}
		if (buttonCode == 32 || buttonCode == 13){
			_accept->execute(state);
		}
		if (buttonCode == 27){
			_accept->undo(state);
		}
	}
	Controller(Command* up, Command* right, Command* accept, Command* additional = NULL)
	: _up(up), _right(right), _accept(accept), _additional(additional)
	{
		#ifdef DEBUG
			std::cout << "Controller was created.\n";
		#endif
	}
	~Controller()
	{
		delete _up;
		delete _right;
		delete _accept;
		delete _additional;

		#ifdef DEBUG
			std::cout << "Controller was deleted.\n";
		#endif
	}
};
///////STATE_FACTORY////////////////////////////////////////////////
class StateFactory
{
public:
	State* createState(std::string, GameCore*, std::string);
	StateFactory()
	{
		#ifdef DEBUG
			std::cout << "StateFactory was created.\n";
		#endif
	}
	~StateFactory()
	{
		#ifdef DEBUG
			std::cout << "StateFactory was deleted.\n";
		#endif
	}
};
///////GAME_CORE////////////////////////////////////////////////
class GameCore
{
	Controller       *control;
	State			 *gameStatesArr,
					 *outdatedState;
	StateFactory     *stFactory;
	CharacterFactory *chFactory;
	GameItemFactory  *giFactory;
	Character        **heroes,
			         **enemies;
	GameItem         **inventory;
	GameItem         **drop;
	bool 			 openSession,
					 refreshPrevent;
	int 	         hLength,
		             eLength,
		             iLength,
		             dLength,
		             blockedSlotQ;
	Chronicle        *_chronicle;
	
	void refresh()
	{
		if(refreshPrevent){
			return;
		}
		#ifndef DEBUG
			clearScreen();
		#endif
		gameStatesArr->refresh();
	}
public:
	std::string getCharacterCatalog(std::string state = "Friendly", int power = 0)
	{
		return chFactory->catalog(state, power);
	}
	Character* createCharacter(std::string name, std::string type = "Classic")
	{
		return chFactory->createCharacter(name, type);
	}
	Character* getCharacter(int pos, bool hero = true)
	{
		Character* result = NULL;
		if(pos >= 0){
			if(hero){
				if(pos < hLength){
					result = heroes[pos];
				}
			} else {
				if(pos < eLength){
					result = enemies[pos];
				}
			}
		}
		return result;
	}
	void setCharacter(Character* character, bool restat = false)
	{
		Character **curArr  = NULL;
		int       curLength = 0;
		bool      fail      = true;
		if(character->state() == "Friendly"){
			if(restat){
				curArr    = enemies;
				curLength = eLength;
				character->state("Enemy");	
			} else {
				curArr    = heroes;
				curLength = hLength;
			}
		} else {
			if(restat){
				curArr = heroes;
				curLength = hLength;
				character->state("Friendly");	
			} else {
				curArr = enemies;
				curLength = eLength;
			}
		}
		for(int i = 0; i < curLength; i++)
		{
			if(!curArr[i]){
				curArr[i] = character;
				fail   = false;
				break;
			}
		}
		if(fail){
			#ifdef DEBUG
				std::cout << "Character set error.\n";
			#endif
			closeSess();
		}
	}
	Character** getCharacterArr(int &length, bool hero = true)
	{
		Character **array = NULL;
		if(hero){
			length = hLength;
			array  = heroes;
		} else {
			length = eLength;
			array  = enemies;
		}
		return array;
	}
	GameItem** getInventory(int &inventoryLength, int &availableLength)
	{
		inventoryLength = iLength;
		availableLength = iLength - blockedSlotQ;
		return inventory;
	}
	GameItem* getInventoryItem(int pos)
	{
		GameItem* result = NULL;
		if( pos >= 0 && pos < (iLength - blockedSlotQ) ){
			result = inventory[pos];
		}
		return result;
	}
	void deleteInventoryItem(int pos)
	{
		if( pos >= 0 && pos < (iLength - blockedSlotQ) ){
			delete inventory[pos];
			inventory[pos] = NULL;
		}
	}
	void useInventoryItem(int itemPos, int characPos, bool hero = true)
	{
		if( itemPos >= 0 && itemPos < (iLength - blockedSlotQ) ){
			int         curArrayLength = 0;
			Character** curArray       = NULL;
			if(hero){
				curArrayLength = hLength;
				curArray       = heroes;
			} else {
				curArrayLength = eLength;
				curArray       = enemies;
			}
			if(characPos < curArrayLength && curArray[characPos]){
				GameItem* returnItem = curArray[characPos]->setItem(inventory[itemPos]);
				chronicle(curArray[characPos]->history(), true);
				inventory[itemPos]   = returnItem;
			}
		}
	}
	void addInventoryItem(GameItem *gi)
	{
		bool full = true;
		for(int i = 0; i < iLength; i++)
		{
			if(!inventory[i]){
				inventory[i] = gi;
				full = false;
				break;
			}
		}
		if(full){
			delete gi;
		}
	}
	GameItem* createItem(std::string itemName, int exp = 50, int val = 1, int chance = 0,std::string effects = "")
	{
		return giFactory->createItem(itemName, exp, val, chance, effects);
	}
	GameItem** getDrop(int &length)
	{
		length = dLength;
		return drop;
	}
	GameItem* getDropItem(int pos)
	{
		if( pos >= 0 && pos < dLength ){
			return drop[pos];
		}
		return NULL;
	}
	bool dropExist()
	{
		bool result = false;
		for(int i = 0; i < dLength; i++)
		{
			if(drop[i]){
				result = true;
				break;
			}
		}
		return result;
	}
	void setDrop(GameItem **extDrop, int extDropLength)
	{
		int resLengthExtDrop = 0,
			resLengthDrop    = 0;
		for(int i = 0; i < extDropLength; i++)
		{
			if(extDrop[i]){
				resLengthExtDrop++;
			}
		}
		for(int i = 0; i < dLength; i++)
		{
			if(drop[i]){
				resLengthDrop++;
			}
		}
		if(resLengthDrop + resLengthExtDrop > dLength){
			int      newDropLength  = dLength + 10;
			GameItem **newDropPlace = new GameItem*[newDropLength];
			for(int i = 0; i < newDropLength; i++)
			{
				newDropPlace[i] = NULL;
				if(i < dLength && drop[i]){
					newDropPlace[i] = drop[i];
					drop[i] = NULL;
				}
			}
			delete [] drop;
			drop    = newDropPlace;
			dLength = newDropLength;
		}
		bool _break = false;
		int  extDropLastPos = 0;
		for(int i = 0; i < dLength; i++)
		{
			if(!drop[i]){
				_break = true;
				for(int j = extDropLastPos; j < extDropLength; j++)
				{
					if(extDrop[j]){
						drop[i]    = extDrop[j];
						extDrop[j] = NULL;
						_break     = false;
						extDropLastPos++;
						break;
					}
					extDropLastPos++;
				}
			}
			if(_break){
				break;
			}
		}
		delete [] extDrop;
	}
	void releaseDrop()
	{
		for(int i = 0; i < dLength; i++)
		{
			delete drop[i];
			drop[i] = NULL;
		}
		delete [] drop;
		dLength = 10;
		drop    = new GameItem*[dLength];
		for(int i = 0; i < dLength; i++)
		{
			drop[i] = NULL;
		}
	}
	void loop()
	{
		while(openSession && gameStatesArr)
		{
			//while(!kbhit());
			control->processor(gameStatesArr);
		}
		#ifdef DEBUG
			std::cout << "Game loop break.\n";
		#endif
	}
	bool setNewState(std::string newState, std::string additional = "", bool replace = false)
	{
		State* newStatePtr = NULL;
		if(outdatedState){
			newStatePtr = outdatedState->search(newState, &outdatedState);
		}
		if(!newStatePtr){
			newStatePtr = stFactory->createState(newState, this, additional);
		}
		if (!newStatePtr){
			#ifdef DEBUG
				std::cout << "New state set error.\n";
			#endif
			closeSess();
			return false;
		}
		newStatePtr->additional(additional);
		if(replace && gameStatesArr){
			State* prevState = gameStatesArr->getPrevState();
			newStatePtr->setPrevState(prevState);
			gameStatesArr->setPrevState(outdatedState);
			outdatedState = gameStatesArr;
			gameStatesArr = newStatePtr;
		} else {
			newStatePtr->setPrevState(gameStatesArr);
			gameStatesArr = newStatePtr;
		}
		refresh();
		return true;
	}
	bool setPrevState()
	{
		State* prevState = gameStatesArr->getPrevState();
		if(prevState){
			gameStatesArr->setPrevState(outdatedState);
			outdatedState = gameStatesArr;
			gameStatesArr = prevState;
			refresh();
			return true;
		}
		#ifdef DEBUG
			std::cout << "State stack is empty.\n";
		#endif
		closeSess();
		return false;
	}
	void closeSess()
	{
		openSession = false;
		#ifdef DEBUG
			std::cout << "Game session was closed\n";
		#endif
	}
	void chronicle(std::string mess = "", bool writeOnly = false)
	{
		if(mess != ""){
			_chronicle->setMessage(mess);
		}
		if(!writeOnly){
			_chronicle->print();
		}
	}
	#ifdef DEBUG
		void stackInfo()
		{
			std::cout << "-StackInfo-\nCurrent: ";
			if(gameStatesArr){
				gameStatesArr->info();
			}
			std::cout << "\nOut: ";
			if(outdatedState){
				outdatedState->info();
			}
			std::cout << "\n";
		}
	#endif
	GameCore(
	Controller/*  */ *controller, 
	StateFactory/**/ *stateFactory, 
	CharacterFactory *charFactory, 
	GameItemFactory  *itemFactory,
	Chronicle/*   */ *chronic
	
	):	control      (controller),
		stFactory    (stateFactory),
		chFactory    (charFactory),
		giFactory    (itemFactory),
		gameStatesArr(NULL), 
		outdatedState(NULL),
		heroes       (NULL),
		enemies      (NULL),
		drop         (NULL),
		inventory    (NULL),
		_chronicle   (chronic),
		openSession  (true),
		hLength      (2),
	    eLength      (3),
	    iLength      (20),
	    dLength      (10),
	    blockedSlotQ (5),
	    refreshPrevent(false)
	{
		heroes = new Character*[hLength];
		for(int i = 0; i < hLength; i++)
		{
			heroes[i] = NULL;
		}
		enemies = new Character*[eLength];
		for(int i = 0; i < eLength; i++)
		{
			enemies[i] = NULL;
		}
		
		inventory = new GameItem*[iLength];
		for(int i = 0; i < iLength; i++)
		{
			inventory[i] = NULL;
		}
		inventory[iLength - 1] = createItem("InventoryBlockedSlot");
		for(int i = 0; i < blockedSlotQ - 1; i++)
		{
			inventory[iLength - 2 - i] = inventory[iLength - 1];
		}
		
		drop = new GameItem*[dLength];
		for(int i = 0; i < dLength; i++)
		{
			drop[i] = NULL;
		}
		
		chronicle("Let's game!", true);
		
		refreshPrevent = true;
		setNewState("Menu");
		refreshPrevent = false;
		
		setNewState("EnemiesCreater");
		#ifdef DEBUG
			std::cout << "GameCore was created.\n";
		#endif
	}
	~GameCore()
	{
		#ifdef DEBUG
			std::cout << "GameCore attempt to delete.\n";
		#endif
		for(int i = 0; i < hLength; i++)
		{
			delete heroes[i];
		}
		delete [] heroes;
		#ifdef DEBUG
			std::cout << "GameCore: heroes deleted\n";
		#endif
		for(int i = 0; i < eLength; i++)
		{
			delete enemies[i];
		}
		delete [] enemies;
		#ifdef DEBUG
			std::cout << "GameCore: enemies deleted\n";
		#endif
		GameItem *blockSlot = NULL;
		for(int i = 0; i < iLength; i++)
		{
			if(inventory[i] && inventory[i]->title() == "InventoryBlockedSlot"){
				blockSlot = inventory[i];
				inventory[i] = NULL;
			}
			delete inventory[i];
		}
		delete blockSlot;
		delete [] inventory;
		for(int i = 0; i < dLength; i++)
		{
			delete drop[i];
		}
		delete [] drop;
		delete outdatedState;
		delete gameStatesArr;
		delete stFactory;
		delete chFactory;
		delete giFactory;
		delete control;
		delete _chronicle;
		#ifdef DEBUG
			std::cout << "GameCore was deleted.\n";
		#endif
	}
};
///////START_SCREEN////////////////////////////////////////////////
class StartScreen: public State
{
public:
	void up()		{close();}
	void down()		{close();}
	void right()	{close();}
	void left()		{close();}
	void accept()	{close();}
	void close()
	{
		gameCore->setNewState("Menu","", true);
	}
	void refresh()
	{	
		printIntro();
		printCenterWord(80, "Press Space to continue...");
	}
	std::string className()
	{
		return "StartScreen";
	}
	StartScreen(GameCore* gc): State(gc)
	{
		#ifdef DEBUG
			std::cout << "StartScreen was created.\n";
		#endif
	}
	~StartScreen()
	{
		#ifdef DEBUG
			std::cout << "StartScreen - ";
		#endif
	}
};
///////DEAD_SCREEN////////////////////////////////////////////////
class DeadScreen: public State
{
public:
	void up()		{close();}
	void down()		{close();}
	void right()	{close();}
	void left()		{close();}
	void accept()	{close();}
	void close()
	{
		gameCore->closeSess();
	}
	void refresh()
	{	
		printGameOver();
		printCenterWord(80, "Press Space to continue...");
	}
	std::string className()
	{
		return "DeadScreen";
	}
	DeadScreen(GameCore* gc): State(gc)
	{
		#ifdef DEBUG
			std::cout << "DeadScreen was created.\n";
		#endif
	}
	~DeadScreen()
	{
		#ifdef DEBUG
			std::cout << "DeadScreen - ";
		#endif
	}
};
///////CHOISE//////////////////////////////////////////////////////
class Choise: public State
{
	std::string _question;
	bool		choisePos;
public:
	void up()
	{
		right();
	}
	void down()
	{
		right();
	}
	void right()
	{
		if(choisePos){
			choisePos = false;
		} else {
			choisePos = true;
		}
		refresh();
	}
	void left()
	{
		right();
	}
	void accept()
	{
		if(choisePos){
			prevState->additional("close");
		} else {
			close();
		}
	}
	void close()
	{
		gameCore->setPrevState();
	}
	void refresh()
	{
		clearScreen();
		std::cout << "\n\n\n\n";
		printCenterWord(80,_question);
		int qLength = _question.length();
		std::string items = "";
		if(choisePos){
			items = centerWord(qLength - 2, "\x10Yes      No");	
		} else {
			items = centerWord(qLength - 2, " Yes     \x10No");	
		}
		items = '\xB6' + items + '\xC7';
		printCenterWord(80,items, '\xC4');
		items = _dLD_ + line(_dD_, qLength - 2) + _dRD_;
		printCenterWord(80,items);
	}
	std::string className()
	{
		return "Choise";
	}
	Choise(GameCore* gc, std::string question): State(gc), _question(""), choisePos(false)
	{
		question = centerWord(question.length() + 4, question, _dU_);
		_question = _dLU_ + question + _dRU_;
		#ifdef DEBUG
			std::cout << "Choise was created.\n";
		#endif
	}
	~Choise()
	{
		#ifdef DEBUG
			std::cout << "Choise - ";
		#endif
	}
};
///////MENU////////////////////////////////////////////////////////
class Menu: public State
{
private:
	 Owner<MenuItem> *gameMenu;
public:
	void up()
	{
		clearScreen();
		printIntro();
		gameMenu->choisePrevItem();
	}
	void down()
	{
		clearScreen();
		printIntro();
		gameMenu->choiseNextItem();
	}
	void left() {}
	void right(){}
	void accept()
	{
		std::string menuItemTitle = gameMenu->getCurrentElementTitle();
		if(menuItemTitle == "Exit"){
			close();
		} else if(menuItemTitle == "Start"){
			gameCore->setNewState("HeroSelect");
		} else {
			gameCore->setNewState(menuItemTitle);
		}
	}
	void close()
	{
		gameCore->setNewState("Choise", "Are you sure to leave?");
	}
	void additional(std::string param)
	{
		if(param == "close"){
			#ifdef DEBUG
				std::cout << "State(Menu) closes the session.\n";
			#endif
			gameCore->closeSess();
		}
		
	}
	void refresh()
	{
		clearScreen();
		printIntro();
		gameMenu->notifyObserver();
	}
	std::string className()
	{
		return "Menu";
	}
	Menu(GameCore *gC, Owner<MenuItem> *gM):State(gC), gameMenu(gM)
	{
		#ifdef DEBUG
			std::cout << "Menu was created.\n";
		#endif
	}
	~Menu()
	{
		delete gameMenu;
		#ifdef DEBUG
			std::cout << "Menu - ";
		#endif
	}
};
///////SHORT_MENU////////////////////////////////////////////////////////
class ShortMenu: public State
{
private:
	 Owner<MenuItem> *gameMenu;
public:
	void up()
	{
		clearScreen();
		std::cout << "\n\n\n\n";
		gameMenu->choisePrevItem();
	}
	void down()
	{
		clearScreen();
		std::cout << "\n\n\n\n";
		gameMenu->choiseNextItem();
	}
	void left() {}
	void right(){}
	void accept()
	{
		std::string menuItemTitle = gameMenu->getCurrentElementTitle();
		gameMenu->choisePrevItem();
		clearScreen();
		if(menuItemTitle == "Main menu"){
			prevState->additional("close");
		} else {
			close();
		}
	}
	void close()
	{
		gameCore->setPrevState();
	}
	void refresh()
	{
		clearScreen();
		std::cout << "\n\n\n\n";
		gameMenu->notifyObserver();
	}
	std::string className()
	{
		return "ShortMenu";
	}
	ShortMenu(GameCore *gC, Owner<MenuItem> *gM):State(gC), gameMenu(gM)
	{
		#ifdef DEBUG
			std::cout << "ShortMenu was created.\n";
		#endif
	}
	~ShortMenu()
	{
		delete gameMenu;
		#ifdef DEBUG
			std::cout << "ShortMenu - ";
		#endif
	}
};
///////CREDITS////////////////////////////////////////////////////
class Credits: public State
{
public:
	void up	   (){}
	void down  (){}
	void left  (){}
	void right (){}
	void accept()
	{
		gameCore->setPrevState();
	}
	void close()
	{
		gameCore->setPrevState();
	}
	void refresh()
	{
		printCenterWord(80, "Game created by Kaitangata.");
		printCenterWord(80, "medgad123@gmail.com");
		printCenterWord(80, "\xC4\xC4\xC4 Dedicated to \xC4\xC4\xC4");
		printCenterWord(80, "my 3D-waifu: Yaroslav Pinchuk");
		printCenterWord(80, "the kid who traded me a hundred: Alexey Sapozhnikov");
		printCenterWord(80, "just a good guy: Denis Gerasimov");
	}
	std::string className()
	{
		return "Credits";
	}
	Credits(GameCore *gC):State(gC)
	{
		#ifdef DEBUG
			std::cout << "Credits was created.\n";
		#endif
	}
	~Credits()
	{
		#ifdef DEBUG
			std::cout << "Credits - ";
		#endif
	}
};
///////ATTACK////////////////////////////////////////////////////
class Attack: public State
{
	bool refreshPrevent;
public:
	void left() {}
	void right(){}
	void up()   {}
	void down() {}
	void additional(std::string param){}
	void accept(){}
	void close()
	{
		gameCore->setPrevState();
	}
	void refresh()
	{
		if(refreshPrevent){
			refreshPrevent = false;
			return;
		}
		int  heroArrLength    = 0,
			 resHeroArrLength = 0,
			 enemyArrLength   = 0;
		bool death            = false;
		
		Character **heroArr  = gameCore->getCharacterArr(heroArrLength);
		Character **enemyArr = gameCore->getCharacterArr(enemyArrLength, false);
		for(int i = 0; i < heroArrLength; i++)
		{
			if(heroArr[i]){
				resHeroArrLength++;
				heroArr[i]->attack();
				gameCore->chronicle(heroArr[i]->history(), true);
				for(int j = 0; j < enemyArrLength; j++)
				{
					if(enemyArr[j]){
						if(!enemyArr[j]->hp()){
							std::string mess = enemyArr[j]->name() + " was killed by a ";
							mess += heroArr[i]->name(); mess += '.';
							gameCore->chronicle(mess, true);
							
							int dropLength = 0;
							GameItem** drop = enemyArr[j]->drop(dropLength);
							gameCore->setDrop(drop, dropLength);
							
							delete enemyArr[j];
							enemyArr[j] = NULL;
							death       = true;
						}
					}
				}
				heroArr[i]->update();
				gameCore->chronicle(heroArr[i]->history(), true);	
			}
		}
		bool enemiesExist = false;
		if(death){
			int resultPos = -1;
			for(int i = 0; i < enemyArrLength; i++)
			{
				if(!enemyArr[i]){
					for(int j = i + 1; j < enemyArrLength; j++)
					{
						if(enemyArr[j]){
							resultPos    = j;
							enemiesExist = true;
							break;
						}
					}
					if(resultPos != -1){
						enemyArr[i]         = enemyArr[resultPos];
						enemyArr[resultPos] = NULL;
						resultPos     = -1;
					} else {
						break;
					}
				} else {
					enemiesExist = true;
				}
			}
		}

		for(int i = 0; i < enemyArrLength; i++)
		{
			if(enemyArr[i]){
				for(int j = 0; j < heroArrLength; j++)
				{
					if(heroArr[j]){
						if(!enemyArr[i]->setTarget(heroArr[j])){
							break;
						}
					}
				}
				enemyArr[i]->attack();
				gameCore->chronicle(enemyArr[i]->history(), true);
			}
		}
		bool heroDeath = true;
		for(int i = 0; i < heroArrLength; i++)
		{
			if(heroArr[i] && heroArr[i]->hp()){
				heroDeath = false;
			}
		}
		if(!enemiesExist && death){
			refreshPrevent = true;
			gameCore->setNewState("EnemiesCreater");
			refreshPrevent = false;
			gameCore->chronicle("Congratulations on clearing the gorge. You moved on, but in front of you waiting...", true);
	
			if(gameCore->dropExist()){
				gameCore->setNewState("DropManager","", true);
				return;
			}
		}
		if(heroDeath){
			gameCore->setNewState("DeadScreen","", true);
			return;
		}
		close();
	}
	std::string className()
	{
		return "Attack";
	}
	Attack(GameCore *gC):State(gC), refreshPrevent(false)
	{
		#ifdef DEBUG
			std::cout << "Attack was created.\n";
		#endif
	}
	~Attack()
	{
		#ifdef DEBUG
			std::cout << "Attack - ";
		#endif
	}	
};
///////ENEMIES_CREATER////////////////////////////////////////////////////
class EnemiesCreater: public State
{
public:
	void left() {}
	void right(){}
	void up()   {}
	void down() {}
	void additional(std::string param){}
	void accept(){}
	void close()
	{
		gameCore->setPrevState();
	}
	void refresh()
	{
		int enemyArrLength = 0,
			enemyArrPos    = 0,
			catalogE0Length= 0,
			catalogE1Length= 0,
			slotFillchance = 40;
		std::string *catalogE0 = strToWordsArr(
			gameCore->getCharacterCatalog("Enemy", 0),
			10,
			catalogE0Length
		);
		std::string *catalogE1 = strToWordsArr(
			gameCore->getCharacterCatalog("Enemy", 1),
			5,
			catalogE1Length
		);
		#ifdef DEBUG
			std::cout << "--EnemiesCreater--\ncatalogE0Length: " << catalogE0Length << "\ncatalogE0Length: "
			<< catalogE1Length << '\n' << "ce0: ";
			for(int i = 0; i < catalogE0Length; i++)
			{
				std::cout << catalogE0[i] << ' ';
			}
			std::cout << "\nce1:";
			for(int i = 0; i < catalogE1Length; i++)
			{
				std::cout << catalogE1[i] << ' ';
			}
			std::cout << '\n';
		#endif
		Character **enemyArr = gameCore->getCharacterArr(enemyArrLength, false);
		#ifdef DEBUG
			if(enemyArr[0]){
				std::cout << "False positive of State(EnemiesCreater) because enemies exists\n";
				close();
				return;
			}
		#endif
		enemyArr[enemyArrPos++] = gameCore->createCharacter(catalogE0[rand() % catalogE0Length]);
		int       chancePerSlot = 0;
		if( (enemyArrLength - 1) > 0 ){
			chancePerSlot = slotFillchance / (enemyArrLength - 1);
		} 
		int shot     = 0,
		    fraction = 0,
		    residue  = 0;
		for(int i = enemyArrPos; i < enemyArrLength; i++)
		{
			#ifdef DEBUG
				std::cout << "fillChance: " << 	slotFillchance << '\n';
			#endif
		    fraction = 100 / slotFillchance;
		    residue  = 100 % slotFillchance;
			if(residue){
				shot = rand() % static_cast<int>((100.0 / slotFillchance) * 100);
				if( shot > 100){
					shot = 0;
				}
			} else {
				shot = !( rand() % fraction );
			}
				 
			if(shot){
				if(rand() % 2){
					enemyArr[enemyArrPos] = gameCore->createCharacter(catalogE1[rand() % catalogE1Length]);
				} else {
					enemyArr[enemyArrPos] = gameCore->createCharacter(catalogE0[rand() % catalogE0Length]);
				}
				enemyArrPos++;
			}
			slotFillchance -= chancePerSlot;
			if(!slotFillchance){
				slotFillchance = 1;
			}
		}
		#ifdef DEBUG
			std::cout << "EnemiesCreater/here1\n";
		#endif
		
		std::string enemyName = "";
		GameItem *functionalItemForEnemy = NULL; //not delete 
		GameItem **enemyDrop             = NULL; //not delete
		for(int i = 0; i < enemyArrLength; i++)
		{
			if(enemyArr[i]){
				enemyName = enemyArr[i]->name();
				enemyDrop = new GameItem*[3];
				
				if(enemyName == "Goblin"){
					functionalItemForEnemy = gameCore->createItem("Claws", 100, rand() % 4 + 1, 20, "Bleeding");
					enemyDrop[0]           = gameCore->createItem("Potion", 0, 0, 50, "Heal"); enemyDrop[0]->title("Heal potion");
					enemyDrop[1]           = gameCore->createItem("Sword", 100, rand() % 5 + 1, 20);enemyDrop[1]->title("Wood sword");
					enemyDrop[2]           = gameCore->createItem("TornCape", 50, 1, 50);
					enemyArr[i]->setItem(functionalItemForEnemy);
				} else if(enemyName == "Cave rat"){
					enemyDrop[0] = gameCore->createItem("RatTail", 20, rand() % 3 + 1, 50);
					enemyDrop[1] = gameCore->createItem("Potion", 0, 0, 50, "DoubleAttack");enemyDrop[1]->title("Double attack");
					enemyDrop[2] = NULL;
					
				} else if(enemyName == "Wisp"){
					std::string burn = "Burn";
					if(rand() % 2){
						burn = "";	
					}
					functionalItemForEnemy = gameCore->createItem("FireBall", 100, rand() % 5 + 1, 30, "Burn");
					enemyDrop[0] = gameCore->createItem("FireBall", 50, rand() % 2 + 1, 50, burn);
					enemyDrop[1] = NULL;
					enemyDrop[2] = NULL;
					enemyArr[i]->setItem(functionalItemForEnemy);
				}
				enemyArr[i]->setDrop(enemyDrop, 3);
				
				functionalItemForEnemy = NULL;
				enemyDrop              = NULL;
			}
		}
		#ifdef DEBUG
			std::cout << "EnemiesCreater/here2\n";
		#endif
		delete [] catalogE0;
		delete [] catalogE1;
		#ifdef DEBUG
			std::cout << "EnemiesCreater/here3\n";
		#endif
		close();
	}
	std::string className()
	{
		return "EnemiesCreater";
	}
	EnemiesCreater(GameCore *gC):State(gC)
	{
		#ifdef DEBUG
			std::cout << "EnemiesCreater was created.\n";
		#endif
	}
	~EnemiesCreater()
	{
		#ifdef DEBUG
			std::cout << "EnemiesCreater - ";
		#endif
	}	
};
//////CHOISE_HERO_TARGET-HERO_CHOISE///////////////////////////////////////////////////////
class CHT_HeroChoise: public State
{
	int length,
		choise,
		arrowHeigth;
		
	std::string *arrow;
public:
	void left(){}
	void right(){}
	void up()
	{
		--choise;
		if(choise < 0){
			choise = length;
		}
		refresh();
	}
	void down()
	{
		++choise;
		if(choise > length){
			choise = 0;
		}
		refresh();
	}
	
	void accept()
	{
		if(choise == length){
			gameCore->setNewState("Attack","", true);
			return;
		}
		std::string param = "h" + toString(choise);
		gameCore->setNewState("CHT_EnemyChoise", param);
	}
	void close()
	{
		gameCore->setPrevState();
	}
	void additional(std::string param)
	{
		choise = 0;
	}
	void refresh()
	{
		clearScreen();
		length = 0;
		printCenterWord(80, "\xC4\xC4\xC4 Heroes \xC4\xC4\xC4");
		int          graphHeigth  = 0,
					 heroArrLength= 0,
					 maxTargets   = 0;
		std::string* graph        = NULL;
		std::string  emptyLine    = line(' ', arrow[0].length());
		Character **heroArr = gameCore->getCharacterArr(heroArrLength);
		Character **targets = NULL;
		for(int i = 0; i < heroArrLength; i++)
		{
			if(heroArr[i]){
				length++;
				graph = statBar(
					heroArr[i]->name(),
					heroArr[i]->hp(),
					heroArr[i]->hp(0, true),
					heroArr[i]->mp(),
					heroArr[i]->mp(0, true),
					choise == i,
					graphHeigth
				);
				for(int j = 0; j < graphHeigth; j++)
				{
					if(choise == i){
						graph[j] = arrow[j] + graph[j] + reverse(arrow[j]);
						targets = heroArr[i]->getTargets(maxTargets);
					} else {
						graph[j] = emptyLine + graph[j] + emptyLine;
					}
					printCenterWord(80,graph[j]);
				}
				delete [] graph;
			}

		}
		std::cout << '\n';
		if(choise == length){
			printCrossedSwords();
		} else {
			printMessageSwords("Choise hero...");
		}
		printCenterWord(80, "\xC4\xC4\xC4 Targets \xC4\xC4\xC4");
		int       enemArrLength= 0;
		bool      targetSet    = false;
		Character **enemArr    = gameCore->getCharacterArr(enemArrLength, false);
		
		for(int i = 0; i < enemArrLength; i++)
		{
			if(enemArr[i]){
				graph = statBar(
					enemArr[i]->name(),
					enemArr[i]->hp(),
					enemArr[i]->hp(0, true),
					enemArr[i]->mp(),
					enemArr[i]->mp(0, true),
					false,
					graphHeigth
				);
				for(int j = 0; j < maxTargets; j++)
				{
					if(targets[j] == enemArr[i]){
						targetSet = true;
					}
				}
				for(int j = 0; j < graphHeigth; j++)
				{
					if(targetSet){
						graph[j] = reverse(arrow[j]) + graph[j] + arrow[j];
					} else {
						graph[j] = emptyLine + graph[j] + emptyLine;
					}
					printCenterWord(80,graph[j]);
				}
				targetSet = false;
				delete [] graph;
			}
		}
	}
	std::string className()
	{
		return "CHT_HeroChoise";
	}
	CHT_HeroChoise(GameCore *gC):State(gC), length(0), choise(0), arrow(NULL), arrowHeigth(3)
	{
		arrow = new std::string[arrowHeigth];
		std::string temp = "000000001000"; decoder(temp); arrow[0] = temp;
		            temp = "011111111110"; decoder(temp); arrow[1] = temp;
		            temp = "000000001000"; decoder(temp); arrow[2] = temp;
		#ifdef DEBUG
			std::cout << "CHT_HeroChoise was created.\n";
		#endif
	}
	~CHT_HeroChoise()
	{
		delete [] arrow;
		#ifdef DEBUG
			std::cout << "CHT_HeroChoise - ";
		#endif
	}	
};
//////CHOISE_HERO_TARGET-ENEMY_CHOISE///////////////////////////////////////////////////////
class CHT_EnemyChoise: public State
{
	int       length,
		      choise,
		      heroPos,
		      arrowHeigth;
	Character *hero;
		
	std::string *arrow;
public:
	void left(){}
	void right(){}
	void up()
	{
		--choise;
		if(choise < 0){
			choise = length - 1;
		}
		refresh();
	}
	void down()
	{
		++choise;
		if(choise == length){
			choise = 0;
		}
		refresh();
	}
	
	void accept()
	{
		int  maxTargets    = 0,
			 setPos        = -1,
			 enemyArrLength= 0;
		bool unsetTarget   = false;
		Character **enemyArr = gameCore->getCharacterArr(enemyArrLength, false);
		if(choise >= enemyArrLength){
			#ifdef DEBUG
				std::cout << "Stat(CHT_EnemyChoise) uncorrect position of enemy.\n";
			#endif
			return;
		}
		Character **targets  = hero->getTargets(maxTargets);
		for(int i = 0; i < maxTargets; i++)
		{
			if( targets[i] ){
				if( targets[i] == enemyArr[choise] ){
					targets[i] = NULL;
					unsetTarget = true;
					break;
				}
			} else {
				setPos = i;
			}
		}
		if(setPos != -1){
			if(!unsetTarget){
				targets[setPos] = enemyArr[choise];
			}
			
		} else if(!unsetTarget){
			return;
		}
		refresh();
	}
	void close()
	{
		gameCore->setPrevState();
	}
	void additional(std::string param)
	{
		if(param.length() != 2){
			#ifdef DEBUG
				std::cout << "Stat(CHT_EnemyChoise) uncorrect param.\n";
			#endif
			close();
		}
		heroPos= param[1] - 48;
		hero   = gameCore->getCharacter(heroPos);
		choise = 0;
	}
	void refresh()
	{
		if(!hero){
			return;
		}
		clearScreen();
		length = 0;
		printCenterWord(80, "\xC4\xC4\xC4 Heroes \xC4\xC4\xC4");
		int          graphHeigth  = 0,
					 heroArrLength= 0,
					 maxTargets   = 0;
		std::string* graph        = NULL;
		std::string  emptyLine    = line(' ', arrow[0].length());
		Character **heroArr = gameCore->getCharacterArr(heroArrLength);
		Character **targets = NULL;
		for(int i = 0; i < heroArrLength; i++)
		{
			if(heroArr[i]){
				graph = statBar(
					heroArr[i]->name(),
					heroArr[i]->hp(),
					heroArr[i]->hp(0, true),
					heroArr[i]->mp(),
					heroArr[i]->mp(0, true),
					heroPos == i,
					graphHeigth
				);
				for(int j = 0; j < graphHeigth; j++)
				{
					if(heroPos == i){
						graph[j]= reverse(arrow[j]) + graph[j] + arrow[j];
						targets = heroArr[i]->getTargets(maxTargets);
					} else {
						graph[j] = emptyLine + graph[j] + emptyLine;
					}
					printCenterWord(80,graph[j]);
				}
				delete [] graph;
			}
		}
		std::cout << '\n';
		printMessageSwords("Choise target(s)...");
		
		printCenterWord(80, "\xC4\xC4\xC4 Targets \xC4\xC4\xC4");
		int       enemArrLength= 0;
		bool      targetSet    = false;
		Character **enemArr    = gameCore->getCharacterArr(enemArrLength, false);
		
		for(int i = 0; i < enemArrLength; i++)
		{
			if(enemArr[i]){
				length++;
				graph = statBar(
					enemArr[i]->name(),
					enemArr[i]->hp(),
					enemArr[i]->hp(0, true),
					enemArr[i]->mp(),
					enemArr[i]->mp(0, true),
					choise == i,
					graphHeigth
				);
				for(int j = 0; j < maxTargets; j++)
				{
					if(targets[j] == enemArr[i]){
						targetSet = true;
					}
				}
				for(int j = 0; j < graphHeigth; j++)
				{
					if(targetSet){
						graph[j] = reverse(arrow[j]) + graph[j] + arrow[j];
					} else {
						if(choise == i){
							graph[j] = arrow[j] + graph[j] + reverse(arrow[j]);
						} else {
							graph[j] = emptyLine + graph[j] + emptyLine;
						}
					}
					printCenterWord(80,graph[j]);
				}
				targetSet = false;
				delete [] graph;
			}
		}
	}
	std::string className()
	{
		return "CHT_EnemyChoise";
	}
	CHT_EnemyChoise(GameCore *gC):State(gC), length(0), choise(0), heroPos(0),arrow(NULL), arrowHeigth(3), hero(NULL)
	{
		arrow = new std::string[arrowHeigth];
		std::string temp = "000000001000"; decoder(temp); arrow[0] = temp;
		            temp = "011111111110"; decoder(temp); arrow[1] = temp;
		            temp = "000000001000"; decoder(temp); arrow[2] = temp;
		#ifdef DEBUG
			std::cout << "CHT_EnemyChoise was created.\n";
		#endif
	}
	~CHT_EnemyChoise()
	{
		delete [] arrow;
		#ifdef DEBUG
			std::cout << "CHT_EnemyChoise - ";
		#endif
	}	
};
//////HERO_SELECT///////////////////////////////////////////////////////
class HeroSelect: public State
{
	int 	  hLength,
		      eLength,
		      choise;
	Character **heroes,
		      **enemies;
	bool	  enemiesExist,
			  setTarget,
			  _close;
public:
	void up()
	{
		if(setTarget){
			int newChoise = 0;
			for(int i = 0; i < hLength; i++)
			{
				if(heroes[i]){
					newChoise = i;
				}
			}
			choise = newChoise;
			setTarget  = false;
		} else {
			choise--;
			if( choise < 0 ){
				if(enemiesExist){
					gameCore->setNewState("EnemySelect", "down", true);
					return;
				} else {
					setTarget = true;
					choise = -2;
				}
			}
		}
		refresh();
	}
	void down()
	{
		if(setTarget){
			if(enemiesExist){
				gameCore->setNewState("EnemySelect", "up", true);
				return;
			} else {
				choise = 0;
			}
			setTarget  = false;
		} else {
			choise++;
			if( !(choise < hLength) || !heroes[choise] ){
				choise = -2;
				setTarget  = true;
			}
		}
		refresh();
	}
	void left(){}
	void right(){}
	void accept()
	{
		if(setTarget){
			gameCore->setNewState("CHT_HeroChoise");//CHT - ChoiseHeroTarget
			return;
		}
		std::string param     = "h",
				    choiseStr = toString(choise);
		if(choiseStr.length() == 1){
			param += '0';
		}
		param += choiseStr;
		gameCore->setNewState("CharInfo", param);
	}
	void close()
	{
		gameCore->setNewState("ShortMenu");
	}
	void additional(std::string param)
	{
		for(int i = 0; i < eLength; i++)
		{
			if(enemies[i]){
				enemiesExist = true;
			}
		}
		if(param == "up"){
			choise = 0;
		} else if(param == "down"){
			choise    = -2;
			setTarget = true;
		} else if(param == "close"){
			_close = true;
			gameCore->setPrevState();
			gameCore->setPrevState();
			_close = false;
		} else {
			choise = 0;
			setTarget = false;
		}
	}
	void refresh()
	{
		if(_close){
			return;
		}
		clearScreen();
		if(!heroes[0]){
			gameCore->setNewState("HeroCatalog");
			return;
		}
		printCenterWord(80, "\xC4\xC4\xC4 Heroes \xC4\xC4\xC4");
		int length = 0;
		std::string* graph = NULL;
		for(int i = 0; i < hLength; i++)
		{
			if(heroes[i]){
				graph = statBar(
					heroes[i]->name(),
					heroes[i]->hp(),
					heroes[i]->hp(0, true),
					heroes[i]->mp(),
					heroes[i]->mp(0, true),
					choise == i,
					length
				);
			} else {
				graph = statBar(
					"[Empty]",
					0,0,0,0,
					false,
					length
				);
			}
			printCenterWord(80,graph[0]);
			if(choise == i){
				graph[1] = "\x10\xC4" + graph[1] + "\xC4\x11";
				graph[1] = centerWord(80, graph[1], '\xC4');
			}
			printCenterWord(80,graph[1]);
			printCenterWord(80,graph[2]);
			delete [] graph;
		}
		graph = button("Set targets", setTarget);
		printCenterWord(80,graph[0]);
		if(setTarget){
			printCenterWord(80,graph[1],'\xC4');
		} else {
			printCenterWord(80,graph[1]);
		}
		printCenterWord(80,graph[2]);
		delete [] graph;
		printCenterWord(80, "\xC4\xC4\xC4 Chronicle \xC4\xC4\xC4");
		gameCore->chronicle();
		printCenterWord(80, "\xC4\xC4\xC4 Enemies \xC4\xC4\xC4");
		for(int i = 0; i < eLength; i++)
		{
			if(enemies[i]){
				graph = statBar(
					enemies[i]->name(),
					enemies[i]->hp(),
					enemies[i]->hp(0, true),
					enemies[i]->mp(),
					enemies[i]->mp(0, true),
					false,
					length
				);
			} else {
				graph = statBar(
					"[Empty]",
					0,0,0,0,
					false,
					length
				);
			}
			printCenterWord(80,graph[0]);
			printCenterWord(80,graph[1]);
			printCenterWord(80,graph[2]);
			delete [] graph;
		}
	}
	std::string className()
	{
		return "HeroSelect";
	}
	HeroSelect(GameCore *gC)
	:State(gC), hLength(0), eLength(0), choise(0), _close(false), setTarget(false), enemiesExist(false)
	{
		heroes  = gameCore->getCharacterArr(hLength);
		enemies = gameCore->getCharacterArr(eLength, false);
		for(int i = 0; i < eLength; i++)
		{
			if(enemies[i]){
				enemiesExist = true;
			}
		}
		#ifdef DEBUG
			std::cout << "HeroSelect was created.\n";
		#endif
	}
	~HeroSelect()
	{
		#ifdef DEBUG
			std::cout << "HeroSelect - ";
		#endif
	}
};
//////DROP_MANAGER///////////////////////////////////////////////////////
class DropManager: public State
{
	GameItem   **drop, 
			   **inventory;
	int        dLength,
			   iLength,
			   iLengthAvaiable,
			   lineLength,
			   choise;
	bool       inventoryMode;
public:
	void left()
	{
		if(choise == 0){
			choise = lineLength - 1;
		} else {
			if( !(choise % lineLength) ){
				choise += lineLength - 1;
			} else {
				--choise;
			}
			if( inventoryMode && choise >= iLengthAvaiable){
				choise = iLengthAvaiable - 1;
			} 
		}
		refresh();
	}
	void right()
	{
		if(choise == lineLength - 1){
			choise -= lineLength - 1;
		} else {
			if( inventoryMode && (choise == iLengthAvaiable - 1) ){
				choise -= (lineLength - (iLength - iLengthAvaiable));
			}
			++choise;
		}
		refresh();
	}
	void up()
	{
		if(choise < lineLength){
			if(inventoryMode){
				inventoryMode = false;
			} else {
				inventoryMode = true;
			}
			choise = 0;
		} else {
			choise -= lineLength;
		}
		refresh();
	}
	void down()
	{
		if(inventoryMode){
			if( choise > (iLength - lineLength) ){
				inventoryMode = false;
				choise = 0;
			} else {
				choise += lineLength;
				if(choise > iLengthAvaiable - 1){
					choise = iLengthAvaiable - 1;
				}
			}
		} else {
			choise += lineLength;
			if( choise >= dLength ){
				inventoryMode = true;
				choise = 0;
			}
		}
		refresh();
	}
	void additional(std::string param)
	{
		if(param == "shift"){
			int shiftItemRecPos= -1,
				recieverLength = iLength;
			GameItem   **source  = drop,
					   **reciever= inventory;
			if(inventoryMode){
				source  = inventory;
				reciever= drop;
				recieverLength = dLength;
			}
			for(int i = 0; i < recieverLength; i++)
			{
				if(!reciever[i]){
					shiftItemRecPos = i;
					break;
				}
			}
			if(shiftItemRecPos != -1){
				reciever[shiftItemRecPos] = source[choise];
				source  [choise]          = NULL;
			}
		}
		drop          = gameCore->getDrop(dLength);
		inventoryMode = false;
		choise        = 0;
	}
	void accept()
	{
		std::string param        = "",
					stringChoise = toString(choise);
		if(inventoryMode){
			param += "i-";
			if(!inventory[choise]){
				return;
			}
		} else {
			param += "d-";
			if(!drop[choise]){
				return;
			}
		}
		if(stringChoise.length() < 2){
			param += '0';
		}
		param += stringChoise; param += '1';
		gameCore->setNewState("ItemInfo", param);
	}
	void close()
	{
		gameCore->releaseDrop();
		gameCore->setPrevState();
	}
	void refresh()
	{
		clearScreen();
		int adaptChoise = 0;
		std::cout << "Congratulations on defeating the monsters. You deserve this award. Choose everything you take with you next...\n";
		if(inventoryMode){
			adaptChoise = -2;
		} else {
			adaptChoise = choise;
		}
		#ifdef DEBUG
			std::cout << "State(DropManager) attempt to print drop\n";
		#endif
		printCenterWord(80,"\xC4\xC4\xC4 Swag \xC4\xC4\xC4");
		printInventory(drop, dLength, adaptChoise, lineLength);
		#ifdef DEBUG
			std::cout << "State(DropManager) success print drop\n";
		#endif
		if(!inventoryMode){
			adaptChoise = -2;
		} else {
			adaptChoise = choise;
		}
		printCenterWord(80,"\xC4\xC4\xC4 Inventory \xC4\xC4\xC4");
		printInventory(inventory, iLength, adaptChoise, lineLength);
		
		printCenterWord(80,"\xC4\xC4\xC4 Attention \xC4\xC4\xC4");
		printCenterWord(80,"All that remains at the top of the screen will be irretrievably destroyed!");
	}
	std::string className()
	{
		return "DropManager";
	}
	DropManager(GameCore *gC)
	:State(gC), drop(NULL), inventory(NULL), dLength(0), iLength(0), iLengthAvaiable(0), choise(0), lineLength(10), inventoryMode(false)
	{
		drop      = gameCore->getDrop(dLength);
		inventory = gameCore->getInventory(iLength, iLengthAvaiable);
		#ifdef DEBUG
			std::cout << "DropManager was created.\n";
		#endif
	}
	~DropManager()
	{
		#ifdef DEBUG
			std::cout << "DropManager - ";
		#endif
	}
};
//////ENEMY_SELECT///////////////////////////////////////////////////////
class EnemySelect: public State
{
	int 	  hLength,
		      eLength,
		      choise;
	Character **heroes,
		      **enemies;
	bool      _close;
public:
	void up()
	{
		--choise;
		if(choise < 0){
			gameCore->setNewState("HeroSelect", "down", true);
			return;
		}
		refresh();	
		
	}
	void down()
	{
		++choise;
		if( !(choise < eLength) || !enemies[choise] ){
			gameCore->setNewState("HeroSelect", "up", true);
			return;
		}
		refresh();
	}
	void left(){}
	void right(){}
	void accept()
	{
		std::string param     = "e",
				    choiseStr = toString(choise);
		if(choiseStr.length() == 1){
			param += '0';
		}
		param += choiseStr;
		gameCore->setNewState("CharInfo", param);
	}
	void close()
	{
		gameCore->setNewState("ShortMenu");
	}
	void additional(std::string param)
	{
		if(param == "up"){
			choise = 0;
		} else if(param == "down"){
			choise = 0;
			for(int i = 0; i < eLength; i++)
			{
				if(enemies[i]){
					choise = i;
				}
			}
		} else if(param == "close"){
			_close = true;
			gameCore->setPrevState();
			gameCore->setPrevState();
			_close = false;
		} else {
			choise = 0;
		}
	}
	void refresh()
	{
		if(_close){
			return;
		}
		clearScreen();
		printCenterWord(80, "\xC4\xC4\xC4 Heroes \xC4\xC4\xC4");
		std::string* graph = NULL;
		int length = 0;
		for(int i = 0; i < hLength; i++)
		{
			if(heroes[i]){
				graph = statBar(
					heroes[i]->name(),
					heroes[i]->hp(),
					heroes[i]->hp(0, true),
					heroes[i]->mp(),
					heroes[i]->mp(0, true),
					false,
					length
				);
			} else {
				graph = statBar(
					"[Empty]",
					0,0,0,0,
					false,
					length
				);
			}
			printCenterWord(80,graph[0]);
			printCenterWord(80,graph[1]);
			printCenterWord(80,graph[2]);
			delete [] graph;
		}
		graph = button("Set targets", false);
		printCenterWord(80,graph[0]);
		printCenterWord(80,graph[1]);
		printCenterWord(80,graph[2]);
		delete [] graph;
		printCenterWord(80, "\xC4\xC4\xC4 Chronicle \xC4\xC4\xC4");
		gameCore->chronicle();
		printCenterWord(80, "\xC4\xC4\xC4 Enemies \xC4\xC4\xC4");
		for(int i = 0; i < eLength; i++)
		{
			if(enemies[i]){
				graph = statBar(
					enemies[i]->name(),
					enemies[i]->hp(),
					enemies[i]->hp(0, true),
					enemies[i]->mp(),
					enemies[i]->mp(0, true),
					choise == i,
					length
				);
			} else {
				graph = statBar(
					"[Empty]",
					0,0,0,0,
					false,
					length
				);
			}
			printCenterWord(80,graph[0]);
			if(choise == i){
				graph[1] = "\x10\xC4" + graph[1] + "\xC4\x11";
				graph[1] = centerWord(80, graph[1], '\xC4');
			}
			printCenterWord(80,graph[1]);
			printCenterWord(80,graph[2]);
			delete [] graph;
		}
	}
	std::string className()
	{
		return "EnemySelect";
	}
	EnemySelect(GameCore *gC)
	:State(gC), choise(0), hLength(0), eLength(0), _close(false)
	{
		heroes  = gameCore->getCharacterArr(hLength);
		enemies = gameCore->getCharacterArr(eLength, false);
		#ifdef DEBUG
			std::cout << "EnemySelect was created.\n";
		#endif
	}
	~EnemySelect()
	{
		#ifdef DEBUG
			std::cout << "EnemySelect - ";
		#endif
	}
};
//////HERO_CATALOG///////////////////////////////////////////////////////
class HeroCatalog: public State
{
	Character   **heroes;
	int         length;
	std::string **characteristicArray;
	int         choise;
public:
	void up(){}
	void down(){}
	void left()
	{
		--choise;
		if(choise < 0){
			choise = length - 1;
		}
		refresh();
	}
	void right()
	{
		++choise;
		if(choise >= length){
			choise = 0;
		}
		refresh();
	}
	void accept()
	{
		gameCore->setCharacter(heroes[choise]);
		heroes[choise] = NULL;
		gameCore->setPrevState();
	}
	void close()
	{
		gameCore->setNewState("Menu", "", true);
	}
	void refresh()
	{
		clearScreen();
		std::cout << "\n\n\n\n";
		printCenterWord(80,"\xC9\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xBB");
		std::string str = "Choose your character\xC7";
		str = "\xB6" + str;
		printCenterWord(80,str, '\xC4');
		printCenterWord(80,"\xC8\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xCD\xBC");
		std::cout << "\n\n\n\n";
		int indent1 = (80 - length*18 - length + 1)/2,
			indent2 = indent1;
		if(indent2 % 2){
			indent2++;
		}
		printLine(' ',indent1);
		for(int i = 0; i < length; i++)
		{
			if(choise == i){
				std::cout << _dLU_;
				printCenterWord(16,heroes[i]->name(),_dU_);
				std::cout << _dRU_;
			} else {
				std::cout << _LU_;
				printCenterWord(16,heroes[i]->name(),_U_);
				std::cout << _RU_;
			}
			if(i < length - 1){
				std::cout << ' ';
			}
		}
		printLine(' ',indent2);
		int _continue = true;
		int k = -1;
		while(_continue)
		{
			k++;
			printLine(' ',indent1);
			for(int i = 0; i < length; i++)
			{
				if(choise == i){
					std::cout << _dL_ << ' ';
				} else {
					std::cout << _L_ << ' ';
				}
				if(!heroes[i]->printLineOfPicture(k)){
					_continue = false;
				}
				if(choise == i){
					std::cout << ' ' << _dR_;
				} else {
					std::cout << ' ' << _R_;
				}
				if(i < length - 1){
					std::cout << ' ';
				}
			}
			printLine(' ',indent2);
		}
		printLine(' ',indent1);
		for(int i = 0; i < length; i++)
		{
			if(choise == i){
				std::cout << '\xCC' << _dU_ << "Characteristic"<< _dU_ << '\xB9';
			} else {
				std::cout << '\xC3' << _U_ << "Characteristic"<< _U_ << '\xB4';
			}
			if(i < length - 1){
				std::cout << ' ';
			}
		}
		printLine(' ',indent2);
		
		for(int i = 0; i < 3; i++)//characteristicArray
		{
			printLine(' ',indent1);
			for(int j = 0; j < length; j++)//heroes
			{
				if(choise == j){
					std::cout << _dL_;
				} else {
					std::cout << _L_;
				}
				printCenterWord(
					16,
					characteristicArray[j][i]
				);
				if(choise == j){
					std::cout << _dR_;
				} else {
					std::cout << _R_;
				}
				if(j < length - 1){
					std::cout << ' ';
				}
			}
			printLine(' ',indent2);
		}
		printLine(' ',indent1);
		for(int i = 0; i < length; i++)
		{
			if(choise == i){
				std::cout << _dLD_;
				printLine(_dD_,16);
				std::cout << _dRD_;
			} else {
				std::cout << _LD_;
				printLine(_D_,16);
				std::cout << _RD_;
			}
			if(i < length - 1){
				std::cout << ' ';
			}
		}
		printLine(' ',indent2);
		std::cout << "\n\n\n\n\n";
	}
	std::string className()
	{
		return "HeroCatalog";
	}
	HeroCatalog(GameCore *gC):State(gC), length(0), choise(0)
	{
		std::string catalogString = gameCore->getCharacterCatalog();//heroOnly
		std::string *catalog   = strToWordsArr(catalogString, 5, length);
		heroes = new Character*[length];
		for(int i = 0; i < length; i++)
		{
			heroes[i] = gameCore->createCharacter(catalog[i]);
		}
		delete [] catalog;
		
		characteristicArray = new std::string*[length];
		int attack = 0,
			defence= 0,
			trash  = 0;
			
		for(int i = 0; i < length; i++)
		{
			heroes[i]->attack (0,attack, trash);
			heroes[i]->defence(0,defence,trash);
			characteristicArray[i]   = new std::string[3];
			characteristicArray[i][0]= "HP/MP: "  + toString(heroes[i]->hp()) + "/" 
												  + toString(heroes[i]->mp());
			characteristicArray[i][1]= "Attack: " + toString(attack);
			characteristicArray[i][2]= "Defence: "+ toString(defence);
		}
		#ifdef DEBUG
			std::cout << "HeroCatalog was created.\n";
		#endif
	}
	~HeroCatalog()
	{
		for(int i = 0; i < length; i++)
		{
			delete [] characteristicArray[i];
			delete heroes[i];
		}
		delete [] heroes;
		delete [] characteristicArray;
		#ifdef DEBUG
			std::cout << "HeroCatalog - ";
		#endif
	}
};
//////CHAR_INFO///////////////////////////////////////////////////////
class CharInfo: public State
{
	GameItem  **itemArr;
	Character *character;
	int       choise,
			  equipLength,
			  resLength,
			  lineLength,
			  characterPos;
	bool      needUpdate,
			  inventNav,
			  inventory;		
public:
	void up()
	{
		if(!resLength && !inventory){
			return;
		}
		bool update = true;
		if(inventNav){
			int newChoise = choise - lineLength;
			if(newChoise < 0){
				if(resLength){
					inventNav = false;
					if(resLength == equipLength){
						newChoise = 0;
					} else {
						newChoise = equipLength;
					}
				} else {
					update    = false;
					newChoise = choise;
				}
			}
			choise = newChoise;
		} else {
			if(choise){
				if(equipLength && choise >= equipLength){
					choise = resLength - equipLength - 1;
					if(choise >= equipLength){
						choise = equipLength - 1;
					}
				} else if(inventory){
					inventNav = true;
					choise    = 0;
				}
			} else if(inventory){
				inventNav = true;
			} else {
				update = false;
			}
		}
		if(update){
			refresh();
		}
	}
	void down()
	{
		if(!resLength && !inventory){
			return;
		}
		bool update = true;
		if(inventNav){
			int newPos = choise + lineLength,
				trash  = 0,
				availableLength = 0;
			gameCore->getInventory(trash, availableLength);
			if((newPos > availableLength)){
				if(resLength){
					inventNav = false;
					newPos = 0;
				} else {
					update = false;
					newPos = choise;
				}
			}
			choise = newPos;
		} else {
			if(choise < equipLength){
				if(equipLength == resLength && inventory){
					inventNav = true;
					choise    = 0;
				} else {
					choise += equipLength;
					if(choise >= resLength){
						choise = resLength - 1;
					}
				}
			} else if(inventory){
				inventNav = true;
				choise    = 0;
			} else {
				update = false;
			}
		}
		if(update){
			refresh();
		}
	}
	void left()
	{
		if(!resLength && !inventory){
			return;
		}
		bool update = true;
		choise--;
		if(choise < 0){
			if(inventNav){
				int availableLength = 0,
					trash           = 0;
				gameCore->getInventory(trash,availableLength);
				choise = availableLength - 1;
			} else {
				if(resLength == 1){
					update = false;
				}
				choise = resLength - 1;
			}
		}
		if(update){
			refresh();
		}
	}
	void right()
	{
		if(!resLength && !inventory){
			return;
		}
		bool update = true;
		choise++;
		if(inventNav){
			int availableLength = 0,
				trash           = 0;
			gameCore->getInventory(trash,availableLength);
			if(choise >= availableLength){
				choise = 0;
			}
		} else {
			if(choise >= resLength){
				choise = 0;
			}
			if(resLength == 1){
				update = false;
			}
		}
		if(update){
			refresh();
		}
	}
	void accept()
	{
		std::string param = "",
					temp  = toString(choise) + '0';
		if( inventory && inventNav && gameCore->getInventoryItem(choise) ){
			param += "i-";
			if(temp.length() != 3){
				param += "0";
			}
			param += temp;
			#ifdef DEBUG
				std::cout << "State(CharInfo) accept param: " << param << '\n';
			#endif
			gameCore->setNewState("ItemInfo", param);
		}
		if(!inventNav && resLength){
			if(character->state() == "Enemy"){
				param += 'e';
			} else {
				param += 'h';
			}
			param += toString(characterPos);
			if(temp.length() != 3){
				param += "0";
			}
			param += temp;
			#ifdef DEBUG
				std::cout << "State(CharInfo) accept param: " << param << '\n';
			#endif
			gameCore->setNewState("ItemInfo", param);
		}
	}
	void close()
	{
		if(!inventNav){
			needUpdate = true;
			choise = 0;
		}
		gameCore->setPrevState();
	}
	void refresh()
	{
		if(!character){
			#ifdef DEBUG
				std::cout << "Character not exists.\n";
			#endif
			close();
			return;
		}
		clearScreen();
		if(needUpdate){
			resLength  = -1;
			itemArr    = NULL;
			equipLength= 0;
			while(character->iterator(++resLength));//poluchaem dlinu massiva elementov personaja
			if(resLength){
				itemArr = new GameItem*[resLength];
				if(!inventory){
					choise = 0;
				}
				
			} else {
				inventNav = true;
				choise    = 0;
			}
			for(int i = 0; i < resLength; i++)
			{
				itemArr[i] = character->iterator(i);
				if(itemArr[i]->type() == "Weapon" || itemArr[i]->type() == "Armour"){
					equipLength++;
				}
			}
			needUpdate = false;
		}	
		
		printCenterWord(80," Character info ", '\xC4');
		std::cout<< _dLU_;
		printLine(_dU_,16);
		std::cout<< '\xCB';
		printLine(_dU_,61);
		std::cout<< _dRU_ << _dL_;
		printCenterWord(16,character->name());
		std::cout<< _dR_;
		printCenterWord(61,"\xC4\xC4 Equipments \xC4\xC4");
		std::cout<< _dR_;
		int characterCardHeight = 30,
			characterCardPos    = 0;
		std::string *characterCard = new std::string[characterCardHeight];
		std::string strTemp = "";
		while(true){
			strTemp = character->getLineOfPicture(characterCardPos);
			if(strTemp == ""){
				break;
			}
			characterCard[characterCardPos] += "\xBA ";
			decoder(strTemp);
			characterCard[characterCardPos] += strTemp;
			characterCard[characterCardPos] += " \xBA";
			++characterCardPos;
		}
		characterCard[characterCardPos] += "\xCC\xCD";
		characterCard[characterCardPos] += "Characteristic\xCD\xB9";
		++characterCardPos;
		
		std::string *characteristicArr = new std::string[3];
		int lowerAtt = 0,
			upperAtt = 0,
			def      = 0,
			equipDef = 0;
		character->attack (0, lowerAtt, upperAtt);
		character->defence(0, def,      equipDef);
		characteristicArr[0] = "HP/MP: "  + toString( character->hp() ) + "/"  + toString( character->mp()     );
		characteristicArr[1] = "Attack: " + toString( lowerAtt )        + '-'  + toString( upperAtt + lowerAtt );
		characteristicArr[2] = "Defence: "+ toString( def + equipDef )  + "(+" + toString( equipDef )      + ')';
		for(int i = 0; i < 3; i++)
		{
			characterCard[characterCardPos] += _dL_;
			characterCard[characterCardPos] += centerWord(16, characteristicArr[i]);
			characterCard[characterCardPos] += _dR_;
			characterCardPos++;
		}
		delete [] characteristicArr;
		int length1   =0,
			length2   =0,
			length12  =0,
			emptyLineQ=0,
			adaptChoise;
			
		if(!inventNav){
			adaptChoise = choise;
		} else {
			adaptChoise = -2;
		}
		
		std::string *arr1 = singleInventoryLine(
			itemArr, 
			equipLength,
			adaptChoise, 
			length1
		);
		std::string *arr2 = singleInventoryLine(
			itemArr   + equipLength,
			resLength - equipLength,
			adaptChoise - equipLength,
			length2
		);
		length12   = length1 + length2 + 1;
		emptyLineQ = characterCardPos - length12;
		if(emptyLineQ > 0){
			length12 += emptyLineQ;
		} else if(emptyLineQ < 0){
			for(int i = 0; i < emptyLineQ; i++)
			{
				characterCard[characterCardPos + i] += _dL_;
				characterCard[characterCardPos + i] += line(' ', 16);
				characterCard[characterCardPos + i] += _dR_;
			}
		}
		std::string *arr3 = new std::string[length12];
		for(int i = 0; i < length1; i++)
		{
			arr3[i] = arr1[i];
		}
		arr3[length1++] = "\xC4\xC4 Effects \xC4\xC4";
		for(int i = 0; i < length2; i++)
		{
			arr3[i + length1] = arr2[i];
		}
		
		for(int i = 0; i < length12; i++)
		{
			std::cout << characterCard[i];
			if(arr3[i] != ""){
				decoder(arr3[i]);
				printCenterWord(61,arr3[i]);
			} else {
				printLine(' ', 61);
			}
			std::cout << _dR_;
		}
		
		std::cout << _dLD_ << line(_dD_,16) << '\xCA' << line(_dD_,61) << _dRD_;
		if(inventory){
			printCenterWord(80," Inventory ", '\xC4');
			int inventLength = 0,
				trash        = 0;
			if(inventNav){
				adaptChoise = choise;
			} else {
				adaptChoise = -2;
			}
			GameItem **inventory = gameCore->getInventory(inventLength, trash);
			printInventory(inventory, inventLength, adaptChoise, lineLength);
		}
		
		delete [] arr1;
		delete [] arr2;
		delete [] arr3;
		delete [] characterCard;
	}
	std::string className()
	{
		return "CharInfo";
	}
	void additional(std::string param)
	{
		bool heroArray,
			 fail = false;
		
		if( param.length() >= 3 ){
			if(param == "use"){
				gameCore->useInventoryItem(choise, characterPos);
			} else if(param == "drop"){
				gameCore->deleteInventoryItem(choise);
			} else {
				if(param[0] == 'h'){
					inventory = true;
					heroArray = true;
				} else if(param[0] == 'e'){
					inventory = false;
					inventNav = false;
					heroArray = false;
				} else {
					fail = true;
				}
				
				if(!fail){
					characterPos = (param[1] - 48)*10 + param[2] - 48;
					character    = gameCore->getCharacter(characterPos, heroArray);
				}
			}
			needUpdate = true;
		} else {
			fail = true;
		}
		if(fail){
			close();
		}
	}
	CharInfo(GameCore *gC)
	:State(gC),
	itemArr(NULL),
	character(NULL),
	choise(0),
	equipLength(0),
	resLength(0),
	lineLength(10),
	characterPos(0),
	needUpdate(true),
	inventNav(false),
	inventory(false)
	{
		#ifdef DEBUG
			std::cout << "CharInfo was created.\n";
		#endif
	}
	~CharInfo()
	{
		delete [] itemArr;
		#ifdef DEBUG
			std::cout << "CharInfo - ";
		#endif
	}
};
//////ITEM_INFO///////////////////////////////////////////////////////
class ItemInfo: public State
{
	int         itemPos,
				cardHeigth,
				choise,
				bLength,
				effectLength;
	GameItem    *item;
	bool        needUpdate,
			    action;
	std::string *itemCard,
				*buttonsTitle;
	std::string	lastRequest;
public:
	void up()
	{
		if(action){
			if(effectLength){
				choise = 0;
				action = false;
				refresh();
			}
		} else {
			choise = bLength / 2;
			action = true;
			refresh();
		}
	}
	void down()
	{
		up();
	}
	void left()
	{
		bool update = true;
		--choise;
		if(choise < 0){
			if(action){
				choise = bLength - 1;
			} else {
				choise = effectLength - 1;
				if(effectLength == 1){
					update = false;
				}
			}
		}
		if(update){
			refresh();
		}
	}
	void right()
	{
		bool update = true;
		++choise;
		if(action){
			if(choise == bLength){
				choise = 0;
			}
		} else {
			if(choise == effectLength){
				choise = 0;
				if(effectLength == 1){
					update = false;
				}
			}
		}
		if(update){
			refresh();
		}
		
	}
	void accept()
	{
		if(action){
			if(buttonsTitle[choise] == "DROP"){//drop
				prevState->additional("drop");
			} else if(buttonsTitle[choise] == "USE"){//use
				prevState->additional("use");
			} else if(buttonsTitle[choise] == "SHIFT"){
				prevState->additional("shift");
				
			}
			needUpdate = true;
			close();
		}
	}
	void close()
	{
		gameCore->setPrevState();
	}
	void refresh()
	{
		if(!item){
			#ifdef DEBUG
				std::cout << "State(ItemInfo) was closed because item not exist.\n";
			#endif
			close();
			return;
		}
		clearScreen();
		if(needUpdate){ //itemCardStor_
			needUpdate = false;
			action     = false;
			choise     = 0;
			delete [] itemCard;
			
			std::string *itemPic       = item->getPicture();
			int         picPathWidth   = itemPic[0].length(),
				        itemTitleLength= ( item->title() ).length();
			if(itemTitleLength > picPathWidth){
				picPathWidth = itemTitleLength;
			}
			
			int picLength = item->getPictureLength();

			std::string *picPath   = new std::string[picLength];
			for(int i = 0; i < picLength; i++)
			{
				picPath[i]  = _dL_;
				decoder(itemPic[i]);
				picPath[i] += centerWord(picPathWidth,itemPic[i]);
				picPath[i] += _dR_;
			}
			std::string *paramPath = new std::string[picLength];
			int paramPathPos   = 0,
				paramPathWidth = 0;
				
			std::string itemType   = item->type(),
						itemSubtype= item->subtype(),
						typeField  = "Type: "    + itemType,
						subtField  = "Subtype: " + itemSubtype,
						strField   = "Strength: ",
						powerField = "";
			if       ( itemType == "Weapon" ){
				powerField = "Attack: ";
			} else if( itemType == "Armour" ){
				powerField = "Defence: ";
			} else {
				powerField = "";
			}
			
			paramPath[paramPathPos++]= typeField;
			if( itemType != "Potion" && itemType != "Effect"){
				paramPath[paramPathPos++]= subtField;
				paramPath[paramPathPos]  = strField;   paramPath[paramPathPos++]+=toString(item->expiration());
				paramPath[paramPathPos]  = powerField; paramPath[paramPathPos++]+=toString(item->value());
			}
			
			paramPathWidth = paramPath[0].length();
			for(int i = 1; i < paramPathPos; i++)
			{
				if(paramPath[i].length() > paramPathWidth){
					paramPathWidth = paramPath[i].length();
				}
			}
			for(int i = 0; i < picLength; i++)
			{
				if(i < paramPathPos){
					paramPath[i] = centerWord(paramPathWidth, paramPath[i]);
				} else {
					paramPath[i]  = line(' ', paramPathWidth);
				}
				paramPath[i] +=_dR_;
			}

			int         discriptPathWidth = 80 - picPathWidth - paramPathWidth - 4;
			std::string *discriptPath = stringDecorator(item->discription(),discriptPathWidth, picLength);
			cardHeigth = picLength + 2;
			itemCard = new std::string[cardHeigth];
			itemCard[0] = _dLU_ + 
			    centerWord( picPathWidth,     item->title(),_dU_ ) + '\xCB' + 
			    centerWord( paramPathWidth,   "Parameters", _dU_ ) + '\xCB' +
			    centerWord( discriptPathWidth,"Discription",_dU_ ) + _dRU_;
			
			itemCard[cardHeigth - 1] = _dLD_ + 
			    line(_dD_, picPathWidth)     + '\xCA' + 
			    line(_dD_, paramPathWidth)   + '\xCA' + 
			    line(_dD_, discriptPathWidth)+ _dRD_;

			for(int i = 0; i < picLength; i++)
			{
				itemCard[i + 1] = "";
				itemCard[i + 1] = picPath[i] + paramPath[i] + discriptPath[i] + _dR_;
			}
			
			delete [] picPath;
			delete [] paramPath;
			delete [] discriptPath;
		} //itemCardStor_end
		
		printCenterWord(80, " Item info ", _U_);
		for(int i = 0; i < cardHeigth; i++)
		{
			std::cout << itemCard[i];
		}
		printCenterWord(80, " Effects ", _U_);
		effectLength = 0;
		ExtItem **effects = item->getEffects(effectLength);
		if( !(effects && effectLength) ){
			printCenterWord(80,"[Empty]");
			if( !action ){
				action = true;
				choise = bLength / 2;
			}
		} else {
			int         effectsLineLength = 0;
			std::string *effectsLine      = NULL;
			if(action){
				effectsLine = singleInventoryLine(effects,effectLength, -2, effectsLineLength);
			} else {
				effectsLine = singleInventoryLine(effects,effectLength, choise, effectsLineLength);
			}
			for(int i = 0; i < effectsLineLength; i++)
			{
				decoder(effectsLine[i]);
				printCenterWord(80, effectsLine[i]);
			}
			if(action){
				std::cout << _dLU_;
				printLine(_dU_, 78);
				std::cout << _dRU_;
				std::cout << _dL_;
				printLine(' ', 78);
				std::cout << _dR_;
				std::cout << _dLD_;
				printLine(_dD_, 78);
				std::cout << _dRD_;
			} else {
				int discriptionHeight = 10,
				    paramWidth        = 10,
				    paramHeight       = 3,
				    tempParamWidth    = 0;
				    
				std::string emptyParamLine = "";
				
				std::string *paramPath   = new std::string[paramHeight];
							paramPath[0] = "Type: "    + effects[choise]->type();
				            paramPath[1] = "Duration: "+ toString(effects[choise]->expiration());
				            paramPath[2] = "Chance: "  + toString(effects[choise]->chance(true));
        		
				tempParamWidth = paramPath[0].length();
				if(paramWidth < tempParamWidth){
					paramWidth = tempParamWidth;
				}
				
				for(int i = 0; i < paramHeight; i++)
				{
					tempParamWidth = paramPath[i].length();
					
					if(paramWidth < tempParamWidth){
						paramWidth = tempParamWidth;
					}
				}
				emptyParamLine = _dL_ + line(' ',paramWidth) + _dR_;
				
				std::string *discriptPath = stringDecorator(effects[choise]->discription(),77 - paramWidth, discriptionHeight);
				
				std::string head = _dLU_ + 
					centerWord(paramWidth,     "Parameters",  _dU_) + '\xCB' + 
					centerWord(77 - paramWidth,"Discription", _dU_) + _dRU_,
							foot = _dLD_ + 
					line(_dD_, paramWidth)      + '\xCA' + 
					line(_dD_, 77 - paramWidth) + _dRD_,
							body = "";
				
				std::cout << head;
				for(int i = 0; i < discriptionHeight; i++)
				{
					if(i < paramHeight){
						body = _dL_ + centerWord(paramWidth, paramPath[i]) + _dR_ + discriptPath[i] + _dR_;
					} else {
						body = emptyParamLine + discriptPath[i] + _dR_;
					}
					std::cout << body;
					body = "";
				}
				std::cout << foot;
				delete [] discriptPath;
				delete [] paramPath;
			}
			delete [] effectsLine;
		}
		
		//printCenterWord(80, " Action ", _U_);
		int adaptChoise = choise;
		if(!action){
			adaptChoise = -2;
		}
		std::string **buttPath = new std::string*[bLength];
		for(int j = 0; j < bLength; j++)
		{
			buttPath[j] = button(buttonsTitle[j],j == adaptChoise);
			buttPath[j][0] += "  ";
			buttPath[j][1] += "\xC4\xC4";
			buttPath[j][2] += "  ";
		}
		std::string temp = "";
		for(int i = 0; i < 3; i++)
		{
			
			for(int j = 0; j < bLength; j++){
				temp += buttPath[j][i];
			}
			if( i != 1 ){
				printCenterWord(80,temp);
			} else {
				printCenterWord(80,temp,'\xC4');
			}
			temp = "";
		}
		
		for(int j = 0; j < bLength; j++)
		{
			delete [] buttPath[j];
		}
		delete [] buttPath;
	}
	void additional(std::string param)
	{
		std::cout << "State(ItemInfo) additional param:" << param << '\n';
		bool fail  = false;

		if(lastRequest == param && !needUpdate){
			return;
		}
		
		needUpdate = true;
		delete [] buttonsTitle;
		buttonsTitle = NULL;
		
		if( param.length() == 5 ){
			int charPos  = param[1] - 48,
				specifier= param[4] - 48;
				
				itemPos = (param[2] - 48)*10 + param[3] - 48;
			if(param[0] == 'h'){
				Character* character = gameCore->getCharacter(charPos);
				if(character){
					item = character->iterator(itemPos);
				}
				bLength        = 1;
				buttonsTitle   = new std::string[bLength];
				buttonsTitle[0]= "OK";
			} else if(param[0] == 'i'){
				item = gameCore->getInventoryItem(itemPos);
				if(specifier){
					bLength         = 2;
					buttonsTitle    = new std::string[bLength];
					buttonsTitle[0] = "OK"; buttonsTitle[1] = "SHIFT";
				} else {
					bLength         = 3;
					buttonsTitle    = new std::string[bLength];
					buttonsTitle[0] = "OK"; buttonsTitle[1] = "USE"; buttonsTitle[2] = "DROP";
				}
			} else if(param[0] == 'e'){
				Character* character = gameCore->getCharacter(charPos,false);
				if(character){
					item = character->iterator(itemPos);
				}
				bLength         = 1;
				buttonsTitle    = new std::string[bLength];
				buttonsTitle[0] = "OK";
			} else if(param[0] == 'd'){
				item = gameCore->getDropItem(itemPos);
				
				bLength         = 2;
				buttonsTitle    = new std::string[bLength];
				buttonsTitle[0] = "OK"; buttonsTitle[1] = "SHIFT";
			} else {
				fail = true;
			}
		} else {
			fail = true;
		}
		if(fail) {
			#ifdef DEBUG
				std::cout << "State(ItemInfo): uncorrect parameter.\n";
			#endif
			close();
		} else {
			lastRequest = param;
		}
		
	}
	std::string className()
	{
		return "ItemInfo";
	}
	ItemInfo(GameCore *gC)
	:State(gC), 
	 itemPos(0),
	 cardHeigth(0),
	 item(NULL),
	 needUpdate(true),
	 itemCard(NULL),
	 choise(0),
	 action(false),
	 buttonsTitle(NULL),
	 bLength(0),
	 lastRequest(""),
	 effectLength(0)
	{
		#ifdef DEBUG
			std::cout << "ItemInfo was created.\n";
		#endif
	}
	~ItemInfo()
	{
		delete [] itemCard;
		delete [] buttonsTitle;
		#ifdef DEBUG
			std::cout << "ItemInfo - ";
		#endif
	}
};
State* StateFactory::createState(std::string state, GameCore* gC, std::string additional = "")
{
	State* st = NULL;

	if(state == "StartScreen"){
		st = new StartScreen(gC);
	} else if(state == "Menu"){
		int length = 3;
		MenuItem* array = new MenuItem[length];
		array[0] = MenuItem("Start");
		array[1] = MenuItem("Credits");
		array[2] = MenuItem("Exit");
		IterObserver<MenuItem>* obs 	= new MenuItemDisplay<MenuItem>	();
		Container<MenuItem>* 	c 		= new Container<MenuItem>		(array, length);
		Owner<MenuItem>* 		gameMenu= new Owner<MenuItem>			(c, obs);
		st = new Menu(gC, gameMenu);
	} else if(state == "ShortMenu"){
		int length = 2;
		MenuItem* array = new MenuItem[length];
		array[0] = MenuItem("Continue");
		array[1] = MenuItem("Main menu");
		IterObserver<MenuItem>* obs 	= new MenuItemDisplay<MenuItem>	();
		Container<MenuItem>* 	c 		= new Container<MenuItem>		(array, length);
		Owner<MenuItem>* 		gameMenu= new Owner<MenuItem>			(c, obs);
		st = new ShortMenu(gC, gameMenu);
	} else if(state == "Credits"){
		st = new Credits(gC);
	} else if(state == "Choise"){
		st = new Choise(gC, additional);
	} else if(state == "HeroSelect"){
		st = new HeroSelect(gC);
	} else if(state == "EnemySelect"){
		st = new EnemySelect(gC);
	} else if(state == "HeroCatalog"){
		st = new HeroCatalog(gC);
	} else if(state == "CharInfo"){
		st = new CharInfo(gC);
	} else if(state == "ItemInfo"){
		st = new ItemInfo(gC);
	} else if(state == "CHT_HeroChoise"){
		st = new CHT_HeroChoise(gC);
	} else if(state == "CHT_EnemyChoise"){
		st = new CHT_EnemyChoise(gC);
	} else if(state == "Attack"){
		st = new Attack(gC);
	} else if(state == "DropManager"){
		st = new DropManager(gC);
	} else if(state == "EnemiesCreater"){
		st = new EnemiesCreater(gC);
	} else if(state == "DeadScreen"){
		st = new DeadScreen(gC);
	}

	#ifdef DEBUG
	if(!st){
		std::cout << "Uncorrect state.\n";
	}
	#endif
	return st;
}
