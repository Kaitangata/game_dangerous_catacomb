#define _LU_ '\xDA'
#define _L_  '\xB3'
#define _LD_ '\xC0'
#define _D_  '\xC4'
#define _RD_ '\xD9'
#define _R_  '\xB3'
#define _RU_ '\xBF'
#define _U_  '\xC4'

#define _dLU_ '\xC9'
#define _dL_  '\xBA'
#define _dLD_ '\xC8'
#define _dD_  '\xCD'
#define _dRD_ '\xBC'
#define _dR_  '\xBA'
#define _dRU_ '\xBB'
#define _dU_  '\xCD'

const int swordHeigth = 16,
	      swordWidth  = 16;
std::string sword[swordHeigth] = {
	"1110000000000000",
	"1221000000000000",
	"1222100000000000",
	"0122210000000000",
	"0012221000000000",
	"0001222100000000",
	"0000122210001100",
	"0000012221013100",
	"0000001222131000",
	"0000000122231000",
	"0000000012310000",
	"0000000133131000",
	"0000001311013100",
	"0000001100001311",
	"0000000000000131",
	"0000000000000111",
};

void clearScreen()
{
	#ifndef DEBUG
		system("cls");
	#endif
}
void printLine(char c, int q = 1)
{
	std::string result = "";
	for (int i = 0; i < q; i++)
	{
		result += c;
	}
	std::cout << result;
}
std::string line(char c, int q = 1)
{
	std::string result = "";
	for (int i = 0; i < q; i++)
	{
		result += c;
	}
	return result;
}
void decoder(std::string &str)
{
	int  length = str.length();
	for(int i = 0; i < length; i++)
	{
		switch(str[i]){
			case '0':
				str[i] = ' ';
			break;
			case '1':
				str[i] = '\xDB';
			break;
			case '2':
				str[i] = '\xB0';
			break;
			case '3':
				str[i] = '\xB1';
			break;
			case '4':
				str[i] = '\xB2';
			break;
		}
	}
}
void printCombination(std::string str)
{
	decoder(str);
	std::cout << str;
}
void repeatCombination(std::string str, int length)
{
	int strLength = str.length();
	int callCount = length / strLength;
	int residue   = length % strLength;
	for (int i = 0; i < callCount; i++)
	{
		printCombination(str);
	}
	if(residue){
		printCombination(str.substr(0,residue));
	}
}
std::string centerWord(int q, std::string str, char placeholder = ' ')
{
	std::string resStr = "";
	int length = str.length();
	if (length < q){
		int temp = (q - length)/2;
		float remains = (q - length)/2.0 - temp;
		for(int i = 0; i < temp; i++)
		{
			resStr += placeholder;
		}
		resStr += str;
		if(remains){
			temp++;
		}
		for(int i = 0; i < temp; i++)
		{
			resStr += placeholder;
		}
		return resStr;
	}
	return str;
}
void printCenterWord(int q, std::string str, char placeholder = ' ')
{
	int length = str.length();
	if (length >= q){
		std::cout << str;
		return;
	} else {
		int temp = (q - length)/2;
		float remains = (q - length)/2.0 - temp;
		printLine(placeholder, temp);
		std::cout << str;
		if(remains){
			printLine(placeholder, temp + 1);
		} else {
			printLine(placeholder, temp);
		}
	}
}
std::string* stringDecorator(std::string string, int transfer,int resArrLength)
{
	std::string  *wordArr= NULL,
				 *resArr	= new std::string[resArrLength];
	int	startWordArrIndex= 0,
		wordArrLength    = 50,
		stopResArrIndex  = 0,
		maxLengthString  = transfer;
	bool lastFit         = false;
	wordArr = strToWordsArr(string, wordArrLength, wordArrLength);
	for(int i = 0; i < resArrLength; i++)
	{
		resArr[i] = "";
		while(startWordArrIndex < wordArrLength - 1){
			stopResArrIndex = i;
			resArr[i] += wordArr[startWordArrIndex++];
			if( resArr[i].length() + 1 <= transfer ){
				resArr[i] += ' ';
			}
			if( (resArr[i].length() + wordArr[startWordArrIndex].length()) > transfer ){
				//if(resArr[i].length() > maxLengthString){
				//	maxLengthString = resArr[i].length();
				//}
				lastFit = false;
				break;
			} else {
				lastFit = true;
			}
		}
	}
	if(lastFit){
		resArr[stopResArrIndex] += wordArr[startWordArrIndex];
	} else if(stopResArrIndex + 1 < resArrLength){
		resArr[stopResArrIndex + 1] += wordArr[startWordArrIndex];
	}
	for(int i = 0; i < resArrLength; i++)
	{
		while(resArr[i].length() < maxLengthString)
		{
			resArr[i] += ' ';
		}
	}
	delete [] wordArr;
	return resArr;
}
void printIntro()
{
	printCombination("               ______                                           ");
	std::cout << '\n';
	printCombination("               |  _  \\                                          ");
	std::cout << '\n';
	printCombination("               | | | |__ _ _ __   __ _  ___ _ __ ___  _   _ ___ ");
	std::cout << '\n';
	printCombination("               | | | / _` | '_ \\ / _` |/ _ | '__/ _ \\| | | / __|");
	std::cout << '\n';
	printCombination("               | |/ | (_| | | | | (_| |  __| | | (_) | |_| \\__ \\");
	std::cout << '\n';
	printCombination("               |___/ \\__,_|_| |_|\\__, |\\___|_|  \\___/ \\__,_|___/");
	std::cout << '\n';
	printCombination("                          _       __/ |                _         ");
	std::cout << '\n';
	printCombination("                         | |     |___/                | |        ");
	std::cout << '\n';
	printCombination("                 ___ __ _| |_ __ _  ___ ___  _ __ ___ | |__  ___ ");
	std::cout << '\n';
	printCombination("                / __/ _` | __/ _` |/ __/ _ \\| '_ ` _ \\| '_ \\/ __|");
	std::cout << '\n';
	printCombination("               | (_| (_| | || (_| | (_| (_) | | | | | | |_) \\__ \\");
	std::cout << '\n';
	printCombination("                \\___\\__,_|\\__\\__,_|\\___\\___/|_| |_| |_|_.__/|___/");
	std::cout << '\n';
	std::cout << '\n';
}
void printGameOver()
{
	const int length = 10;
	std::string lineArr[length];
	lineArr[0] = "  \xDC\xDB\xDB\xDB\xDB \xDC\xDC\xDC      \xDB\xDB\xDB\xDC \xDC\xDB\xDB\xDB\xB2\xDB\xDB\xDB\xDB\xDB     \xB1\xDB\xDB\xDB\xDB\xDB  \xDB\xDB\xB1   \xDB\xB2\xDB\xDB\xDB\xDB\xDB \xDB\xDB\xDF\xDB\xDB\xDB  ";
	lineArr[1] = " \xDB\xDB\xB1 \xDF\xDB\xB1\xDB\xDB\xDB\xDB\xDC   \xB2\xDB\xDB\xB1\xDF\xDB\xDF \xDB\xDB\xB2\xDB   \xDF    \xB1\xDB\xDB\xB1  \xDB\xDB\xB2\xDB\xDB\xB0   \xDB\xB2\xDB   \xDF\xB2\xDB\xDB \xB1 \xDB\xDB\xB1";
	lineArr[2] = "\xB1\xDB\xDB\xB0\xDC\xDC\xDC\xB1\xDB\xDB  \xDF\xDB\xDC \xB2\xDB\xDB    \xB2\xDB\xDB\xB1\xDB\xDB\xDB      \xB1\xDB\xDB\xB0  \xDB\xDB\xB1\xB2\xDB\xDB  \xDB\xB1\xB1\xDB\xDB\xDB  \xB2\xDB\xDB \xB0\xDC\xDB \xB1";
	lineArr[3] = "\xB0\xB2\xDB  \xDB\xDB\xB0\xDB\xDB\xDC\xDC\xDC\xDC\xDB\xDB\xB1\xDB\xDB    \xB1\xDB\xDB\xB1\xB2\xDB  \xDC    \xB1\xDB\xDB   \xDB\xDB\xB0 \xB1\xDB\xDB \xDB\xB0\xB1\xB2\xDB  \xDC\xB1\xDB\xDB\xDF\xDF\xDB\xDC  ";
	lineArr[4] = "\xB0\xB1\xB2\xDB\xDB\xDB\xDF\xB1\xB2\xDB   \xB2\xDB\xDB\xB1\xDB\xDB\xB1   \xB0\xDB\xDB\xB0\xB1\xDB\xDB\xDB\xDB\xB1   \xB0 \xDB\xDB\xDB\xDB\xB2\xB1\xB0  \xB1\xDF\xDB\xB0 \xB0\xB1\xDB\xDB\xDB\xDB\xB0\xDB\xDB\xB2 \xB1\xDB\xDB\xB1";
	lineArr[5] = " \xB0\xB1   \xB1 \xB1\xB1   \xB2\xB1\xDB\xB0 \xB1\xB0   \xB0  \xB0\xB0 \xB1\xB0 \xB0   \xB0 \xB1\xB0\xB1\xB0\xB1\xB0   \xB0 \xDE\xB0 \xB0\xB0 \xB1\xB0 \xB0 \xB1\xB2 \xB0\xB1\xB2\xB0";
	lineArr[6] = "  \xB0   \xB0  \xB1   \xB1\xB1 \xB0  \xB0      \xB0\xB0 \xB0  \xB0     \xB0 \xB1 \xB1\xB0   \xB0 \xB0\xB0  \xB0 \xB0  \xB0 \xB0\xB1 \xB0 \xB1\xB0";
	lineArr[7] = "\xB0 \xB0   \xB0  \xB0   \xB1  \xB0      \xB0     \xB0      \xB0 \xB0 \xB0 \xB1      \xB0\xB0    \xB0    \xB0\xB0   \xB0 ";
	lineArr[8] = "      \xB0      \xB0  \xB0      \xB0     \xB0  \xB0       \xB0 \xB0       \xB0    \xB0  \xB0  \xB0     ";
	lineArr[9] = "                                                 \xB0                 ";
	for(int i = 0; i < length; i++)
	{
		printCenterWord(80, lineArr[i]);
	}
}
void printButton(int line, std::string &word, bool choised)
{
	int width = word.length();
	
	if(line == 0){
		if(choised){
			std::cout << _dLU_;
			printLine(_dU_, width);
			std::cout << _dRU_ << ' ';
		} else {
			std::cout << _LU_;
			printLine(_U_, width);
			std::cout << _RU_<< ' ';
		}
	} else if(line == 1){
		if(choised){
			std::cout << '\xB6' << word << '\xC7' << '\xC4';
		} else {
			std::cout << '\xB4' << word << '\xC3' << '\xC4';
		}
	} else {
		if(choised){
			std::cout << _dLD_;
			printLine(_dD_, width);
			std::cout << _dRD_<< ' ';
		} else {
			std::cout << _LD_;
			printLine(_D_, width);
			std::cout << _RD_<< ' ';
		}
	}
}
std::string* button(std::string word, bool choised)
{
	std::string *result = new std::string[3];
	int  width = word.length();
	char lu    = _dLU_,ru = _dRU_, v = _dU_, ld = _dLD_, rd = _dRD_, tl = '\xB6', tr = '\xC7';
	if(!choised){
		 lu    = _LU_, ru = _RU_, v = _U_, ld = _LD_, rd = _RD_, tl = '\xB4', tr = '\xC3';
	}
	
	result[0] = lu; result[0] += line(v, width); result[0] += ru;
	result[1] = tl; result[1] += word;           result[1] += tr;
	result[2] = ld; result[2] += line(v, width); result[2] += rd;
	return result;
}
std::string bar(int value, int maxValue, bool toLeft = true)
{
	int  step = maxValue/20;
	char full = '\xB2',
		 empty= '\xB0';
		 
	std::string bar = "";
	if(toLeft && maxValue){
		char temp = full;
		     full = empty;
		     empty= temp;
		     value= maxValue - value;
	}
	if(maxValue == value){
		char ch = '0';
		if(maxValue){
			ch = full;
		} else {
			ch = empty;
		}
		for(int i = 0; i < 20; i++)
		{
			bar += ch;
		}
		return bar;
	}
	if(maxValue > 0 && maxValue < 20){
		step = 20 / maxValue;
		int iter = 0;
		for(int i = 0; i < 20; i++)
		{
			iter++;
			if(value > 0){
				bar += full;
				if(iter == step){
					--value;
					iter = 0;
				}
			} else {
				bar += empty;
			}
		}
	} else {
		for(int i = 0; i < 20; i++)
		{
			if(value > 0){
				bar += full;
				value -= step;
			} else {
				bar += empty;
			}
		}
	}
	
	return bar;
}
std::string* statBar(std::string word, int hp, int maxHp, int mp, int maxMp, bool choised, int &length)
{
	length = 3;
	std::string *result = new std::string[length];
	int  width = word.length();
	char lu    = _dLU_,ru = _dRU_, v = _dR_, g = _dU_, ld = _dLD_, rd = _dRD_, tl = '\xB6', tr = '\xC7', td = '\xCB', tu = '\xCA';
	if(!choised){
		 lu    = _LU_, ru = _RU_, v = _R_, g = _U_, ld = _LD_, rd = _RD_, tl = '\xB4', tr = '\xC3', td = '\xC2', tu = '\xC1';;
	}
	result[0] = lu + centerWord(47, word, g) + ru;
	std::string stempMP = toString(mp),
				stempHP = toString(hp);

	result[1] = tl + bar(mp, maxMp) + line(' ',3 - stempMP.length()) + stempMP + td + stempHP + line(' ', 3 - stempHP.length()) +
	bar(hp, maxHp, false) + tr;
	result[2] = ld + line(g, 23) + tu + line(g, 23) + rd;
	
	return result;
}
void printCrossedSwords()
{
	int         crossStart = 8,
	            lineLength = 0,
				copyLinePos= 0;
	std::string resultLine;
	
	resultLine = reverse(sword[0]) + line('0', crossStart);
	lineLength = resultLine.length();

	for(int j = crossStart; j < lineLength; j++)
	{
		if(sword[0][copyLinePos] != '0')
		{
			resultLine[j] = sword[0][copyLinePos];
		}
		copyLinePos++;
		
	}
	decoder(resultLine);
	printCenterWord(80,resultLine);
	for(int i = 1; i < swordHeigth; i++)
	{
		copyLinePos= 0;
		resultLine = reverse(sword[i]) + line('0', crossStart);
		
		for(int j = crossStart; j < lineLength; j++)
		{
			if(sword[i][copyLinePos] != '0')
			{
				resultLine[j] = sword[i][copyLinePos];
			}
			copyLinePos++;
		}
		decoder(resultLine);
		printCenterWord(80,resultLine);
	}
	
}
void printMessageSwords(std::string message)
{
	int firstIter     = swordWidth / 2,
		centerField   = 80 - 2 * swordWidth;
	std::string printStr= "";
	for(int i = 0; i < firstIter; i++)
	{
		printStr = reverse(sword[i]) + line(' ', centerField) + sword[i];
		decoder(printStr);
		std::cout << printStr;
	}
	printStr = reverse(sword[firstIter]) + centerWord(centerField, message) + sword[firstIter];
	decoder(printStr);
	std::cout << printStr;
	for(int i = firstIter + 1; i < swordWidth; i++)
	{
		printStr = reverse(sword[i]) + line(' ', centerField) + sword[i];
		decoder(printStr);
		std::cout << printStr;
	}
}
