class Character
{
protected:
	std::string _name,
				_state,
				_history;
	int         _hp,
				_maxHP,
	            _mp,
	            _maxMP,
		        _tLength,
		        _tLengthMax,
		        _baseAttack,
				_baseDefence,
				_eLength,
				_pLength,
				_dLength;
	GameItem*   _equipments;
	GameItem**  _drop;
	Character** _targets;
	std::string*_picture;
	std::string intelligenceLevel;
public:
	std::string name()
	{
		return _name;
	}
	void info()
	{
		std::cout << "-----------CHAR_INFO-----------\nName: " << _name 
		<< "\nState: " << _state 
		<< "\nHP: "    << _hp 
		<< "\nMP: "    << _mp
		<< "\nAttack: "<< _baseAttack
		<< "\nDefence: "<<_baseDefence
		<< "\nTargetQ: "<<_tLength
		<< "\nItem: ";
		
		for(int i = 0; i < _eLength; i++)
		{
			std::cout << "\n    "<<_equipments[i].title() << "(" << _equipments[i].expiration() << ")"; 
			_equipments[i].info_buff();
		}
		std::cout << "\nTarget(s): ";
		for(int i = 0; i < _tLength; i++)
		{
			if(_targets[i]){
				std::cout << "\n    "<< _targets[i]->name();
			}
		}
		std::cout << "\n-------------------------------\n";
	}
	std::string history()
	{
		std::string str = _history;
		_history = "";
		return  str;
	}
	std::string state(std::string state = "")
	{
		if(state != ""){
			_state = state;
		}
		return _state;
	}
	int hp(int value = 0, bool max = false)
	{
		int result = 0;
		if(_hp < 0){
			_hp = 0;
		}
		if(_hp > _maxHP){
			_hp = _maxHP;
		}
		if(max){
			result = _maxHP;
			_maxHP+= value;
		} else {
			result = _hp;
			_hp   += value;
		}
		return result;
	}
	int mp(int value = 0, bool max = false)
	{
		int result = 0;
		if(max){
			result = _maxMP;
			_maxMP+= value;
		} else {
			result = _mp;
			_mp   += value;
		}
		if(_mp < 0){
			_mp = 0;
		}
		if(_mp > _maxMP){
			_mp = _maxMP;
		}
		return result;
	}
	void attack(int value, int &lower, int &upper)
	{
		GameItem *weapon = search("Weapon");
		std::string weaponSubt = "";
		lower = _baseAttack;
		upper = 0;
		if(weapon){
			weaponSubt = weapon->subtype();
			upper      = weapon->value();
			if(_name == "Warrior"){
				if(weaponSubt == "Magic"){
					lower = 1;
				}
			} else {
				if(weaponSubt != "Magic"){
					upper -= upper / 3;
				}
			}
		}
		
		if(value){
			_baseAttack += value;
		}
	}
	void defence(int value, int &base, int &equip)
	{
		base = _baseDefence;
		GameItem *armour = search("Armour");
		if(armour){
			equip = armour->value();
		} else {
			equip = 0;
		}
		if(value){
			_baseDefence += value;
		}
	}
	void addTargetSlot()
	{
		if(_tLength < _tLengthMax){
			_tLength++;
		}
	}
	void deleteTargetSlot()
	{
		if(_tLength > 1){
			_tLength--;
		}
	}
	bool setTarget(Character* enemy = NULL)
	{
		int  currentLength= _tLengthMax;
		if( intelligenceLevel == "Player" || intelligenceLevel == "Agro"){
			currentLength = _tLength;
		}
		for(int i = 0; i < currentLength; i++)
		{
			if (!_targets[i]){
				if(enemy){
					_targets[i] = enemy;
				}
				return true;
			} 
		}
		return false;
	}
	Character** getTargets(int &maxLength)
	{
		maxLength = _tLength;
		return _targets;
	}
	void attack()
	{
		#ifdef DEBUG
    		std::cout << "void Character("<<_name << ")::attack (entried)\n";
    	#endif
		if(!_targets[0]){
			_history = "A " + _name + " has no targets to attack.";
			return;
		}
		Character *initiator = NULL;
		if(intelligenceLevel == "Player"){
			initiator = this;
		}
		
		int 		damageValue = _baseAttack,
					weaponPower = 0;
		std::string weaponTitle = "",
					weaponType  = "",
					message     = "";
		GameItem*   debuff      = NULL;
		GameItem*   weapon      = search("Weapon");
		
		if (weapon){
			weaponType   = weapon->subtype();
			weaponPower  = weapon->value();
			if( weaponType == "Magic" ){
				if(_mp){
					if( _name == "Warrior" ){
						_mp -= weaponPower * 2;
						damageValue = rand() % weaponPower + 1;
					} else {
						_mp -= weaponPower / 2;
						damageValue += rand() % weaponPower;
					}
				} else {
					if(_state == "Friendly"){
						message     = _name + " doesn't have enough mana points to attack with this weapon. The attack was made by improvised means.";
						weaponTitle = "";
					}
				}
			} else {
				if( _name == "Warrior" ){
					damageValue += rand() % weaponPower;
				} else {
					damageValue += rand() % (weaponPower - (weaponPower / 3));
				}
			}
			weaponTitle  = weapon->title();
			debuff       = weapon->debuff();
			weapon->destroy();
		}
		if(intelligenceLevel == "Weak"){
			#ifdef DEBUG
    			std::cout << "WeekLogic attempt to calculate\n";
    		#endif
			int weakPos = 0,
				weakVal = 0,
				curWV   = 0;
			for(int i = 0; i < _tLengthMax; i++)
			{	
				if(i < _tLength){
					weakPos = i;
					if(!_targets[i]){
						continue;
					}
					weakVal = _targets[i]->hp();
					for(int j = i + 1; j < _tLengthMax; j++)
					{
						if(_targets[j]){
							curWV = _targets[j]->hp();
							if(curWV < weakVal){
								weakVal = curWV;
								weakPos = j;
							}
						}
					}
					if(weakPos != i){
						Character *temp = _targets[i];
						_targets[i]       = _targets[weakPos];
						_targets[weakPos] = temp;
					}
				} else {
					_targets[i] = NULL;
				}
			}
			#ifdef DEBUG
    			std::cout << "WeekLogic success\n";
    		#endif
		} else if(intelligenceLevel == "Random"){
			#ifdef DEBUG
    			std::cout << "RandomLogic attempt to calculate\n";
    		#endif
			Character *targetTemp        = NULL;
			Character **availableTargets = new Character*[_tLengthMax];
			int availableTargetsResLength = 0;
			for(int i = 0; i < _tLengthMax; i++)
			{
				if(_targets[i]){
					availableTargets[availableTargetsResLength++] = _targets[i];
				}
			}
			if(availableTargetsResLength >= 1){
				#ifdef DEBUG
    				std::cout << "RandomLogic one target\n";
    			#endif
				if(availableTargetsResLength == 1){
					_targets[0] = availableTargets[0];
					for(int i = 1; i < _tLengthMax; i++)
					{
						_targets[i] = NULL;
					}
				} else {
					#ifdef DEBUG
    					std::cout << "RandomLogic more then one target\n";
    				#endif
					int newPos = 0;
					for(int i = 0; i < availableTargetsResLength; i++)
					{
						newPos           = rand() % availableTargetsResLength;
						targetTemp       = availableTargets[newPos];
						availableTargets[newPos] = availableTargets[i];
						availableTargets[i]      = targetTemp;
					}
					for(int i = 0; i < _tLengthMax; i++)
					{
						if(i < _tLength && i < availableTargetsResLength){
							_targets[i] = availableTargets[i];
						} else {
							_targets[i] = NULL;
						}
					}
				}
			} 
			delete [] availableTargets;
			#ifdef DEBUG
    			std::cout << "RandomLogic success\n";
    		#endif
		}
		for(int i = 0; i < _tLength; i++)
		{
			if(_targets[i]){
				message += "The " + _name + " attacked the " + _targets[i]->name();
				if(weaponTitle != ""){
					message += " with " + weaponTitle;
				}
				message += _targets[i]->getHit(damageValue, debuff, initiator); message += '\n';
				_targets[i] = NULL;
			}
		}
		_history = message;
		#ifdef DEBUG
    		std::cout << "void Character("<<_name << ")::attack (exiting)\n";
    	#endif
	}
	std::string getHit(int value, GameItem* effects = NULL, Character *initiator = NULL)
	{
		#ifdef DEBUG
    		std::cout << "void Character("<<_name << ")::getHit (entried)\n";
    	#endif
		if(initiator && intelligenceLevel == "Agro"){
			setTarget(initiator);
		}
		std::string message      = "";
		int         damageValue  = value - _baseDefence;
		GameItem*   armour       = search("Armour");
		
		if (armour){
			damageValue -= armour->value();
			armour->destroy();
		}
		if(damageValue <= 0){
			damageValue = 1;
		}
		_hp -= damageValue;
		message = " and caused him damage equal to " + toString(damageValue) + " point(s).";
		
		if(effects){
			#ifdef DEBUG
    			std::cout << "attempt to set new effects\n";
    		#endif
			setItem(effects);
			#ifdef DEBUG
    			std::cout << "effects has set\n";
    		#endif
			message += _history;
		}
		#ifdef DEBUG
    		std::cout << "void Character("<<_name << ")::getHit (exiting)\n";
    	#endif
		return message;
	}
	GameItem** drop(int &length)
	{
		if( !_drop || !_dLength ){
			length = 0;
			return NULL;
		}
		
		int        resultPos= 0;
		GameItem** result   = new GameItem*[_dLength];
		for(int i = 0; i < _dLength; i++)
		{
			result[resultPos] = NULL;
			if(_drop[i] && _drop[i]->chance()){
				result[resultPos++] = _drop[i];
				_drop[i] = NULL;
			}
		}
		length = resultPos;
		return result;
	}
	void setDrop(GameItem** drop, int dLength)
	{
		_drop    = drop;
		_dLength = dLength;
	}
	GameItem* search(std::string searchValue, std::string searchField = "Type")
	{
		GameItem* gameItem = NULL;
		if(_equipments){
			if(searchField == "Type"){
				for(int i = 0; i < _eLength; i++)
				{
					if(_equipments[i].type() ==  searchValue){
						gameItem = _equipments + i;
						break;
					}
				}	
			} else if(searchField == "Title"){
				for(int i = 0; i < _eLength; i++)
				{
					if(_equipments[i].title() ==  searchValue){
						gameItem = _equipments + i;
						break;
					}
				}
			}
		}
		return gameItem;
	}
	GameItem* iterator(int pos)
	{
		if(pos >= _eLength){
			return NULL;
		}
		return _equipments + pos;
	}
	GameItem* setItem(GameItem *item)
	{
		#ifdef DEBUG
    		std::cout << "GameItem* Character::setItem() (entried)\n";
    	#endif
		GameItem *retItem     = NULL,
		  		 *searchResult= NULL;
		bool 	    _return     = false;
		std::string message     = "";
		if(item){
			std::string setItemType  = item->type();
			std::string setItemTitle = item->title();
			if(_equipments){
				if(setItemType == "Weapon" || setItemType == "Armour"){
					searchResult= search(setItemType);
					_return     = true;
				} else {
					searchResult = search(setItemTitle, "Title");
				}
				if(searchResult){
					searchResult->release(this);
					if(_return){
						 retItem     = new GameItem("Empty");
						*retItem     = *searchResult;	
					}
					*searchResult= *item;
					message = searchResult->update(this);
				} else {
					GameItem* newEquipArr   = new GameItem[_eLength + 1];
					int       newEquipArrPos= 0;
					if(item->type() == "Weapon" || item->type() == "Armour"){
						newEquipArr[newEquipArrPos++] = *item;
						message = newEquipArr[0].update(this);
					} else {
						newEquipArr[_eLength] = *item;
						message = newEquipArr[_eLength].update(this);
					}
					for(int i = 0; i < _eLength; i++)
					{
						newEquipArr[newEquipArrPos++] = _equipments[i];
					}
					_eLength++;
					delete [] _equipments;
					_equipments = newEquipArr;
				}
			} else {
				_eLength = 1;
				_equipments = new GameItem[_eLength];
				_equipments[0] = *item;
				message = _equipments[0].update(this);
			}

			if(setItemType == "Weapon" || setItemType == "Armour"){
				_history = "The " + _name + " was equipped with a \"" + setItemTitle + "\"";
			} else if(setItemType == "Potion"){
				_history = "The " + _name + " had used a \"" + setItemTitle + "\"";
			} else if(setItemType == "Effect") {
				_history = "The " + _name + " receive \"" + setItemTitle + "\"";
			}
			_history += message;
			_history += ".";
		}
		#ifdef DEBUG
    		std::cout << "GameItem* Character::setItem() attempt to delete item\n";
    	#endif
		delete item;
		#ifdef DEBUG
    		std::cout << "GameItem* Character::setItem() (exiting)\n";
    	#endif
		return retItem;
	}
	void update()
	{
		std::string message   = "",
					tempMess  = "";
		bool overexposure     = false;
		int  resLength        = 0;
		int *newArrEquipIndex = NULL;
		if(_eLength){
			newArrEquipIndex  = new int[_eLength];
		}
		for(int i = 0; i < _eLength; i++)
		{
			tempMess = "\"" + _equipments[i].title() + "\"";
			tempMess += _equipments[i].update(this) + ", ";
			if(!_equipments[i].expiration()){
				message += tempMess;
				_equipments[i].release(this);
				overexposure = true;
			} else {
				newArrEquipIndex[resLength++] = i;
			}
		}
		if(overexposure){
			GameItem* newArrEquip = NULL;
			if(resLength){
				newArrEquip = new GameItem[resLength];
				for(int i = 0; i < resLength; i++)
				{
					newArrEquip[i] = _equipments[newArrEquipIndex[i]];
				}
			}
			delete [] _equipments;
			delete [] newArrEquipIndex;
			_equipments = newArrEquip;
			_eLength = resLength;
			_history = "The following item(s) were destroyed at the entrance of the attack: " 
			+ message.substr(0, message.length() - 2) + ".\n";
		}
	}
	bool printLineOfPicture(int pos)
	{
		if( _picture && (pos < _pLength) && (pos >= 0) ){
			printCombination(_picture[pos]);
			if(pos < _pLength - 1){
				return true;
			} else {
				return false;
			}
		} 
		return false;
	}
	std::string getLineOfPicture(int pos)
	{
		if( _picture && (pos < _pLength) && (pos >= 0) ){
			return _picture[pos];
		} 
		return "";
	}
	Character(
		std::string name, 
		std::string state,
		int/*    */ HP,
		int/*    */ MP,
		int/*    */ attackPower,
		int/*    */ defence= 0,
		GameItem*   equipments= NULL,
		int/*    */ eLength= 0,
		std::string*picture= NULL,
		int/*    */ picLength= 0,
		std::string inteLevel= "Player"
	):  _name      (name),
		_state     (state),
		_hp        (HP),
		_maxHP     (HP),
		_mp        (MP),
		_maxMP     (MP),
		_tLength   (1),
		_tLengthMax(10),
		_equipments(equipments),
		_eLength   (eLength),
		_drop      (NULL),
		_dLength   (0),
		_picture   (picture),
		_pLength   (picLength),
		_baseAttack(attackPower),
		_baseDefence(defence),
		_history   (""),
		intelligenceLevel(inteLevel)
	{
		
		_targets = new Character*[_tLengthMax];
		for(int i = 0; i < _tLengthMax; i++)
		{
			_targets[i] = NULL;
		}
		#ifdef DEBUG
    		std::cout << "Character("<< _name <<") was created.\n";
    	#endif
	}
	~Character()
	{
		#ifdef DEBUG
    		std::cout << "Character("<< _name <<"): attempt to delete char\n";
    	#endif
		delete [] _targets;
		delete [] _equipments;
		#ifdef DEBUG
    		std::cout << "Character("<< _name <<"): _equipments deleted.\n";
    	#endif
		for(int i = 0; i < _dLength; i++)
		{
			delete _drop[i];
		}
		delete [] _drop;
		#ifdef DEBUG
    		std::cout << "Character("<< _name <<") was deleted.\n";
    	#endif
	}
};
class CharacterFactory
{
private:
	std::string _catalogH,
				_catalogE0,
				_catalogE1;
	std::string *warriorPic,
				*magePic,
				*goblinPic,
				*ratPic,
				*wispPic;
	int         picHeight;
public:
	std::string catalog(std::string state = "Friendly", int power = 0)
	{
		if(state == "Friendly"){
			return _catalogH;
		} else {
			if(power == 1){
				return _catalogE1;
			} else {
				return _catalogE0;
			}
		}
		
	}
	Character* createCharacter(std::string name, std::string type = "Classic")
	{
		Character* character = NULL;
		if(type == "Classic"){
			if       ( name == "Warrior" ){
				character = new Character("Warrior","Friendly", 100, 20, 4, 1, NULL, 0, warriorPic, picHeight, "Player");
			} else if( name == "Mage" ){
				character = new Character("Mage",   "Friendly", 80, 50,  2, 0, NULL, 0, magePic,    picHeight, "Player");
			} else if( name ==  "Goblin" ){
				character = new Character("Goblin", "Enemy",    15, 0,   3, 0, NULL, 0, goblinPic,  picHeight, "Random");
			} else if( name == "CaveRat" ){
				character = new Character("Cave rat","Enemy",   10, 0,   2, 1, NULL, 0, ratPic,     picHeight, "Random");
			} else if ( name ==  "Wisp" ){
				character = new Character("Wisp",    "Enemy",   20, 10,  2, 0, NULL, 0, wispPic,    picHeight, "Weak");
			}
		} else {
			//random enemy
		}
		#ifdef DEBUG
		if(!character)
    		std::cout << "CharacterFactory: character with name: " << name << " not exist\n";
    	#endif
		return character;
	}
	CharacterFactory()
	:_catalogH("Warrior Mage"), _catalogE0("Goblin CaveRat"), _catalogE1("Wisp"), picHeight(12)
	{
		warriorPic = new std::string[picHeight];warriorPic[0]  = "00100000000100";warriorPic[1]  = "00110000001100";warriorPic[2]  = "00111011011100";warriorPic[3]  = "01111111111110";warriorPic[4]  = "11111111111111";warriorPic[5]  = "10110000001101";warriorPic[6]  = "00010111101000";warriorPic[7]  = "00110011001100";warriorPic[8]  = "10110011001101";warriorPic[9]  = "01111000011110";warriorPic[10] = "00111111111100";warriorPic[11] = "00001111110000";
		magePic    = new std::string[picHeight];magePic[0]  = "00000000000000";magePic[1]  = "00011111111000";magePic[2]  = "00111111111100";magePic[3]  = "01100000000110";magePic[4]  = "11111100111111";magePic[5]  = "11111111111111";magePic[6]  = "10101011010101";magePic[7]  = "00101111110100";magePic[8]  = "01101011010110";magePic[9]  = "01111011011110";magePic[10] = "00011111111000";magePic[11] = "00000111100000";
		goblinPic  = new std::string[picHeight];goblinPic[0]  = "00000000000000";goblinPic[1]  = "00000000000000";goblinPic[2]  = "00001111110000";goblinPic[3]  = "00011000011000";goblinPic[4]  = "00110000001100";goblinPic[5]  = "00111111111100";goblinPic[6]  = "01101100110110";goblinPic[7]  = "01000100100010";goblinPic[8]  = "01110000001110";goblinPic[9]  = "00111100111100";goblinPic[10] = "00101111110100";goblinPic[11] = "00001000010000";
		ratPic     = new std::string[picHeight];ratPic[0]  = "00001101111000";ratPic[1]  = "00001213333100";ratPic[2]  = "00000133030100";ratPic[3]  = "00000133333310";ratPic[4]  = "00001333333310";ratPic[5]  = "00001333311120";ratPic[6]  = "00013333232000";ratPic[7]  = "00013333333000";ratPic[8]  = "00013311333100";ratPic[9]  = "00013333133100";ratPic[10] = "00013333133100";ratPic[11] = "22221112211200";
		wispPic    = new std::string[picHeight];wispPic[0]  = "00000011321000";wispPic[1]  = "00001333331000";wispPic[2]  = "00011322300000";wispPic[3]  = "01010322313100";wispPic[4]  = "01311113112300";wispPic[5]  = "01211111113100";wispPic[6]  = "01331331332310";wispPic[7]  = "01113323332210";wispPic[8]  = "00112222200200";wispPic[9]  = "00012000000200";wispPic[10] = "00003300002000";wispPic[11] = "00000033330000";
		
		#ifdef DEBUG
    		std::cout << "CharacterFactory was created.\n";
    	#endif	
	}
	~CharacterFactory()
	{
		delete [] warriorPic;
		delete [] magePic;
		delete [] goblinPic;
		delete [] ratPic;
		delete [] wispPic;
		#ifdef DEBUG
    		std::cout << "CharacterFactory was deleted.\n";
    	#endif
	}
};
