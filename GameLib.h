#define  HQ_MAX 2
#define  EQ_MAX 3
#include "Item.h"
#include "Iterator.h"
#include "Container.h"
#include "CollectionOwner.h"
#include "Character.h"
std::string StatChange::capture(Character *character)
{
	if(character){
		int trash;
		if(_targetStat == "MP"){
			character->mp(_value);
		} else if(_targetStat == "BA"){
			character->attack(_value, trash, trash);	
		} else if(_targetStat == "BD"){
			character->defence(_value, trash, trash);	
		} else {
			character->hp(_value);	
		}
	}
	return "";
}
std::string StatChange::release(Character *character)
{
	if(character && !_pass){
		int trash;
		if(_targetStat == "MP"){
			character->mp(-_value);
		} else if(_targetStat == "BA"){
			character->attack(-_value, trash, trash);	
		} else if(_targetStat == "BD"){
			character->defence(-_value, trash, trash);	
		} else {
			character->hp(-_value);	
		}
		_pass = true;
	}
	return "";
}
std::string StatChange::update(Character *character)
{
	if(_expiration && _pass){
		if(_pass && character){
			capture(character);
		}
		if(!_repeatable){
			_pass = false;
		}
		if(_destructible){
			_expiration--;
		}
	} else {
		_pass = false;	
	}
	return "";
}
std::string TargetSlotChange::capture(Character *character)
{
	if(character){
		bool add = true;
		if(_value < 0){
			bool add = true;
		}
		for(int i = 0; i < _value; i++)
		{
			if(add){
				character->addTargetSlot();
			} else {
				character->deleteTargetSlot();
			}
		}
	}
	return "";
}
std::string TargetSlotChange::release(Character *character)
{
	if(character){
		bool add = true;
		if(_value > 0){
			add = false;
		}
		for(int i = 0; i < _value; i++)
		{
			if(add){
				character->addTargetSlot();
			} else {
				character->deleteTargetSlot();
			}
		}
	}
	return "";
}
std::string TargetSlotChange::update(Character *character)
{
	if(_expiration && _pass){
		if(_pass && character){
			capture(character);
		}
		if(!_repeatable){
			_pass = false;
		}
		if(_destructible){
			_expiration--;
		}
	} else {
		_pass = false;	
	}
	return "";
}
std::string GameItem::update(Character* character)
{
	if(!_expiration){
		return "";
	}
	std::string message      = "";
	if(character){
		for(int i = 0; i < _eLength; i++)
		{
			if(_effects[i]->subtype() == "Positive"){
				_effects[i]->update(character);
				message += ' ' + _effects[i]->title() + ',';
			} else {
				message += ' ' + _effects[i]->title() + ',';
			}
		}
	}
	if(message.length() > 1){
		message = "(" + message.substr(1, message.length() - 2) + ")";
	}
	if(_type == "Effect"){
		_expiration--;
	}
	if(_type == "Potion"){
		_type  = "Effect";
		_title = "Effect potion \"" + _title + '\"';
		_discription = "Obtained in the investigation of drunk potions.";
		_expiration--;
	}
		return message;

}
template<class TYPE>
std::string* singleInventoryLine(TYPE **array, int arrayLength, int choise, int &resArrLength, int auxiliaryLength = 5)
{
	
	if(!(array && arrayLength)){
		std::string *resArr = new std::string[1];
		resArrLength = 1;
		resArr[0] = "[Empty]";
		return resArr;
	}
	if(choise == -1){
		choise--;
	}
	resArrLength = 0;
	std::string *resArr = new std::string[10];
	std::string tempStr = "";
	if(choise == 0){
		resArr[resArrLength] = "\xC9\xCD\xCD\xCD\xCD\xCD\xBB";
	} else {
		resArr[resArrLength] = "\xDA\xC4\xC4\xC4\xC4\xC4";
	}
	for(int i = 1; i < arrayLength; i++)
	{
		if(i == choise){
			resArr[resArrLength] += "\xC9\xCD\xCD\xCD\xCD\xCD\xBB";
		} else if(i - 1 != choise){
			resArr[resArrLength] += "\xC2\xC4\xC4\xC4\xC4\xC4";
		} else {
			resArr[resArrLength] += "\xC4\xC4\xC4\xC4\xC4";
		}
	}
	if(arrayLength - 1 != choise){
		resArr[resArrLength] += _RU_;
	}
	resArrLength++;//11111111111111111111111111111111111111111111
	resArr[resArrLength] = "";
	int freezeResArrLength = resArrLength;
	for(int i = 0; ;i++)
	{
		if(array[0]){
			if(array[0]->getLineOfPicture(i) == ""){
				break;
			}	
		} else if(i == auxiliaryLength){
			break;
		}
		for(int j = 0; j < arrayLength; j++)
		{
			if(array[j]){
				tempStr = array[j]->getLineOfPicture(i);
			} else {
				tempStr = line(' ', 5);
			}
			if(j == choise){
				resArr[i + freezeResArrLength] += _dL_;
				resArr[i + freezeResArrLength] += tempStr;
				resArr[i + freezeResArrLength] += _dR_;
			} else {
				if(j - 1 != choise){
					resArr[i + freezeResArrLength] += _L_;
				}
				resArr[i + freezeResArrLength] += tempStr;	
				
				if(j + 1 == arrayLength){
					resArr[i + freezeResArrLength] += _R_;
				}	
			}
		}
		resArrLength++;//next_n_line
	}
	
	if(choise == 0){//n
		resArr[resArrLength] = "\xC8\xCD\xCD\xCD\xCD\xCD\xBC";
	} else {
		resArr[resArrLength] = "\xC0\xC4\xC4\xC4\xC4\xC4";
		
	}
	for(int i = 1; i < arrayLength; i++)
	{
		if(i == choise){
			resArr[resArrLength] += "\xC8\xCD\xCD\xCD\xCD\xCD\xBC";
		} else {
			if(i - 1 != choise){
				resArr[resArrLength] += '\xC1';
			}
			resArr[resArrLength] += "\xC4\xC4\xC4\xC4\xC4";		
		}
	}
	if(choise != arrayLength - 1){
		resArr[resArrLength] += _RD_;
	}
	resArrLength++;//n+1
	return resArr;
}
void printInventory(GameItem** inventory, int inventoryLength, int choise, int lineLength, int addPicLength = 5)
{
	int lineQ          = inventoryLength / lineLength,
		lastLineLength = inventoryLength % lineLength,
		lineHeight     = 0,
		tempLength     = 0;
		
	std::string **inventoryLineArr = NULL;
	std::string strUp   = "",
				strDown = "";
	bool        _exit   = false;
	////////////////////////////////safe mode
	bool       needDelete = false;
	int        oldLength = inventoryLength;
	GameItem** oldInvent = inventory;
	if(inventoryLength % 2){
		inventoryLength--;
	}
	int dev = inventoryLength % lineLength;
	if(dev){
		inventoryLength += lineLength % dev;
		inventory = new GameItem*[inventoryLength];
		for(int i = 0; i < inventoryLength; i++)
		{
			if(i < oldLength){
				inventory[i] = oldInvent[i];
			} else {
				inventory[i] = NULL;
			}
		}
		needDelete = true;
		lineQ          = inventoryLength / lineLength;
		lastLineLength = 0;
	}
	/////////////////////////////////safe mode
	if(lastLineLength){
		lineQ++;
	} else {
		lastLineLength = lineLength;
	}
	
	if(lineQ || lastLineLength){
		inventoryLineArr = new std::string*[lineQ];
	} else {
		std::cout << "Error";
		return;
	}
	
	if(lineQ == 1){
		inventoryLineArr[0] = singleInventoryLine(inventory, inventoryLength, choise, lineHeight, addPicLength);
	} else {
		for(int i = 0; i < lineQ - 1; i++)
		{
			inventoryLineArr[i] = singleInventoryLine(
				inventory + i*lineLength,
				lineLength,
				choise - i*lineLength,
				lineHeight,
				addPicLength);
		}
		inventoryLineArr[lineQ - 1] = singleInventoryLine(
			inventory + (lineQ - 1)*lineLength,
			lastLineLength,
			choise - (lineQ - 1)*lineLength,
			lineHeight,
			addPicLength);
	}
	for(int i = 1; i < lineQ; i++)
	{
		strUp      = inventoryLineArr[i][0];
		tempLength = strUp.length();
		for(int j = 0; j < tempLength; j++)
		{
			if(strUp[j] == '\xC2'){
				strUp[j] = '\xC5';
			}
			if(strUp[j] == _LU_){
				strUp[j] = '\xC3';
			}
			if(strUp[j] == _RU_){
				strUp[j] = '\xB4';
			}
		}
		inventoryLineArr[i - 1][lineHeight - 1] = strUp;
	}
	
	for(int i = 0; i < lineQ; i++)
	{
		if(_exit){
			break;
		}
		strUp      = inventoryLineArr[i][lineHeight - 2];
		strDown    = inventoryLineArr[i][lineHeight - 1];
		tempLength = strUp.length();
		for(int j = 0; j < tempLength; j++)
		{
			if(strUp[j] == _dL_){
				strDown[j] = _dLD_;
				while(strUp[++j] != _dR_)
				{
					strDown[j] = _dD_;	
				}
				strDown[j] = _dRD_;
				inventoryLineArr[i][lineHeight - 1] = strDown;
				_exit = true;
				break;
			}
		}
	}
	printCenterWord(80, inventoryLineArr[0][0]);
	for(int i = 0; i < lineQ; i++)
	{
		for(int j = 1; j < lineHeight; j++)
		{
			decoder(inventoryLineArr[i][j]);
			printCenterWord(80, inventoryLineArr[i][j]);
		}
		delete [] inventoryLineArr[i];
	}
	std::cout << '\n';
	delete [] inventoryLineArr;
	if(needDelete){
		delete [] inventory;
	}
}
class GameItemFactory
{
	std::string *slotBlockPic,
	            *swordPic,
	            *bleedingPic,
	            *indigestionPic,
				*powerUpPic,
				*healPic,
				*potionPic,
				*capePic,
				*clawsPic,
				*caveFeverPic,
				*ratTailPic,
				*burnPic,
				*fireBallPic,
				*targetPic;
	int         picHeight;
public:
	GameItem* createItem(std::string itemName, int exp = 50, int val = 1, int chance = 0, std::string effectsStr = "")
	{
		GameItem* result = NULL;
		if(itemName == "InventoryBlockedSlot"){
			result = new GameItem(
				"InventoryBlockedSlot",//title
				"",	        //discription
				"",	        //type
				"",			//subtype 
				0, 0, 0,	// expiration/value/chance
				false,		//destructible
				NULL, 0,	// effects / length
				slotBlockPic, picHeight // picture / length
			);
		} else if(itemName == "Sword"){
			result = new GameItem(
				"Sword",							//title
				"Simple sword. Ideal for skinning.",//discription
				"Weapon",							//type
				"Physical",			//subtype 
				exp, val, chance,   // expiration/value/chance
				true,				//destructible
				NULL, 0,			// effects / length
				swordPic, picHeight // picture / length
			);
		} else if(itemName == "Potion"){
			result = new GameItem(
				"Potion",				   //title
				"Contains some effect.",   //discription
				"Potion",				   //type
				"Positive",			//subtype 
				0, 0, chance,       // expiration/value/chance
				false,				//destructible
				NULL, 0,			// effects / length
				potionPic, picHeight // picture / length
			);
		} else if(itemName == "TornCape"){
			result = new GameItem(
				"Torn cape",		 //title
				"But not the blower.",//discription
				"Armour",			//type
				"Physical",			//subtype 
				exp, val, chance,   // expiration/value/chance
				true,				//destructible
				NULL, 0,			// effects / length
				capePic, picHeight // picture / length
			);
		} else if(itemName == "Claws"){
			result = new GameItem(
				"Claws",		     //title
				"Belong to little nimble creatures.",//discription
				"Weapon",			//type
				"Physical",			//subtype 
				exp, val, chance,   // expiration/value/chance
				false,				//destructible
				NULL, 0,		   // effects / length
				clawsPic, picHeight // picture / length
			);
		} else if(itemName == "RatTail"){
			result = new GameItem(
				"Rat tail",		     //title
				"Large and massive can be used as a lash.",//discription
				"Weapon",			//type
				"Physical",			//subtype 
				exp, val, chance,   // expiration/value/chance
				false,				//destructible
				NULL, 0,		   // effects / length
				ratTailPic, picHeight // picture / length
			);
		} else if(itemName == "FireBall"){
			result = new GameItem(
				"Fire ball",		     //title
				"You don't have to be a dragon to spit fire. Spends mana.",//discription
				"Weapon",			//type
				"Magic",			//subtype 
				exp, val, chance,   // expiration/value/chance
				false,				//destructible
				NULL, 0,		   // effects / length
				fireBallPic, picHeight // picture / length
			);
		}
		if(result && effectsStr != ""){
			int         arrEfflength   = 0,
			            arrEffResLength= 0;
			std::string *effTitArray   = strToWordsArr(effectsStr, 10, arrEfflength);
			std::string tempDiscription= "";
			ExtItem**   arrEff         = NULL;
			ExtItem*    effect         = NULL;
			int         effectValue    = 0;
			if(arrEfflength){
				arrEff = new ExtItem*[arrEfflength];
				for(int i = 0; i < arrEfflength; i++)
				{
					if(effTitArray[i] == "Bleeding"){
						effect = new StatChange(
							"Bleeding", //title
							"The wounds of the enemy opened and from them blood began to ooze. Each turn the enemy will lose 1 health point(s).",			//discription
							"Effect",	//type
							"Negative",	//subtype
							5, -1, 30,	//duration/value/chance
							true,true,  //destructible/repeatable
							"HP",		//targetStat
							bleedingPic,//picture
							picHeight	//pLength
						);
					} else if(effTitArray[i] == "Indigestion"){
						effect = new StatChange(
							"Indigestion", //title
							"The opponent feels a lack of energy. The health is temporarily reduced by 5 point(s).", //discription
							"Effect",	//type
							"Negative",	//subtype
							5, -5, 60,	//duration/value/chance
							true,false,  //destructible/repeatable
							"HP",		//targetStat
							indigestionPic,//picture
							picHeight	//pLength
						);
					} else if(effTitArray[i] == "Heal"){
						effect = new StatChange(
							"Instant heal", //title
							"Inspiration that's what the character feels after drinking a potion of treatment. Health replenished by 10 point(s).",			//discription
							"Effect",	//type
							"Positive",	//subtype
							1, 10, 100,	//duration/value/chance
							true,true,  //destructible/repeatable
							"HP",		//targetStat
							healPic,    //picture
							picHeight	//pLength
						);
					} else if(effTitArray[i] == "Burn"){
						effect = new StatChange(
							"Burn", //title
							"Someone became a victim of games with fire. Each turn health is reduced by one points.",//discription
							"Effect",	//type
							"Negative",	//subtype
							5, -1, 20,   //duration/value/chance
							true,true,  //destructible/repeatable
							"MP",		//targetStat
							burnPic,//picture
							picHeight	//pLength
						);
					} else if(effTitArray[i] == "DoubleAttack"){
						effect = new TargetSlotChange(
							"Double attack", //title
							"Blood lust gives you a second pair of hands, don't want to know what are you going to do with them, but now the maximum number of targets increased by 1.",//discription
							"Effect",	//type
							"Positive",	//subtype
							1, 1, 100,   //duration/value/chance
							true,false,  //destructible/repeatable
							targetPic,//picture
							picHeight	//pLength
						);
					}
					if(effect){
						arrEff[arrEffResLength++] = effect;
						effect = NULL;
					}
				}
			}
			if(arrEffResLength){
				result->setEffects(arrEff,arrEffResLength);
			}
			delete [] effTitArray;
		}
		return result;
	}
	GameItemFactory(): picHeight(5)
	{
		slotBlockPic  = new std::string[picHeight];slotBlockPic[0] = "22222";slotBlockPic[1] = slotBlockPic[0];slotBlockPic[2] = slotBlockPic[0];slotBlockPic[3] = slotBlockPic[0];slotBlockPic[4] = slotBlockPic[0];
		swordPic      = new std::string[picHeight]; swordPic[0] = "00100";swordPic[1] = "00100";swordPic[2] = "00100";swordPic[3] = "01110";swordPic[4] = "00100";
		bleedingPic   = new std::string[picHeight];bleedingPic[0] = "00100";bleedingPic[1] = "01110";bleedingPic[2] = "11211";bleedingPic[3] = "01110";bleedingPic[4] = "00100";
		indigestionPic= new std::string[picHeight];indigestionPic[0] = "01110";indigestionPic[1] = "10001";indigestionPic[2] = "00100";indigestionPic[3] = "10001";indigestionPic[4] = "01110";
		powerUpPic    = new std::string[picHeight];powerUpPic[0] = "00100";powerUpPic[1] = "01110";powerUpPic[2] = "10101";powerUpPic[3] = "00100";powerUpPic[4] = "00100";
		healPic       = new std::string[picHeight];healPic[0] = "00000";healPic[1] = "11011";healPic[2] = "11111";healPic[3] = "01110";healPic[4] = "00100";
		capePic 	  = new std::string[picHeight];capePic[0] = "01111";capePic[1] = "11111";capePic[2] = "11110";capePic[3] = "01010";capePic[4] = "01000";
		potionPic     = new std::string[picHeight];potionPic[0] = "00200";potionPic[1] = "00100";potionPic[2] = "01210";potionPic[3] = "12221";potionPic[4] = "11211";
		clawsPic      = new std::string[picHeight];clawsPic[0] = "00000";clawsPic[1] = "10001";clawsPic[2] = "10101";clawsPic[3] = "10101";clawsPic[4] = "01110";
		caveFeverPic  = new std::string[picHeight];caveFeverPic[0] = "01110";caveFeverPic[1] = "12221";caveFeverPic[2] = "01110";caveFeverPic[3] = "01010";caveFeverPic[4] = "10101";
		ratTailPic    = new std::string[picHeight];ratTailPic[0] = "01100";ratTailPic[1] = "10010";ratTailPic[2] = "10000";ratTailPic[3] = "01100";ratTailPic[4] = "00111";
		burnPic       = new std::string[picHeight];burnPic[0] = "22222";burnPic[1] = "23232";burnPic[2] = "22222";burnPic[3] = "23332";burnPic[4] = "22222";
		fireBallPic   = new std::string[picHeight];fireBallPic[0] = "00000";fireBallPic[1] = "01110";fireBallPic[2] = "12221";fireBallPic[3] = "01110";fireBallPic[4] = "00000";
		
		targetPic    = new std::string[picHeight];
		targetPic[0] = "10001";
		targetPic[1] = "01010";
		targetPic[2] = "00100";
		targetPic[3] = "01010";
		targetPic[4] = "10001";
		#ifdef DEBUG
    		std::cout << "GameItemFactory was created.\n";
    	#endif	
	}
	~GameItemFactory()
	{
		delete [] slotBlockPic; delete [] swordPic;  delete [] bleedingPic; delete [] indigestionPic; delete [] powerUpPic;
		delete [] healPic;      delete [] potionPic; delete [] capePic;     delete [] clawsPic;       delete [] caveFeverPic;
		delete [] ratTailPic;   delete [] burnPic;   delete [] fireBallPic; delete [] targetPic;
				
		#ifdef DEBUG
    		std::cout << "GameItemFactory was deleted.\n";
    	#endif
	}
};

#include "GameCore.h"
